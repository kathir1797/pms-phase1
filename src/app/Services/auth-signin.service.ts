import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { User, login, ContactUs, ChangePassword } from '../common_models/user_model';

@Injectable({
  providedIn: 'root'
})
export class AuthSigninService {

  constructor(private sf: ServiceFactoryService) { }

  login(user: login) {
    return this.sf.callRequest('POST', 'rootuserlogin', user);
  }

  contactus(contactForm: ContactUs) {
    return this.sf.callRequest('POST', 'addcontact', contactForm);
  }

  resetPassword(password: ChangePassword){
    return this.sf.callRequest('POST', 'userpwdreset', password);
  }

}
