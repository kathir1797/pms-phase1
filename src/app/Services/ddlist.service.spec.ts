/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DdlistService } from './ddlist.service';

describe('Service: Ddlist', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DdlistService]
    });
  });

  it('should ...', inject([DdlistService], (service: DdlistService) => {
    expect(service).toBeTruthy();
  }));
});
