import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { apiResultObj } from '../common_models/company_model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DdlistService {

  constructor(private sf: ServiceFactoryService) { }


  getDropDown() {
    return this.sf.callRequest('GET', 'dropdown');
  }

  getDDlist(dd: string): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'ddlist', JSON.stringify({ p_dd_name: dd }))
  }

  stateDropDown(code: string, search: string): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'stateddlist', JSON.stringify({ scc: code, search: search }));
  }

  cityDropDown(code: string, search: string): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'citylist', JSON.stringify({ scc: code, search: search }));
  }

  countyDropDown(code: string, search: string): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'countylist', JSON.stringify({ scc: code, search: search }));
  }

  countryDropDown(code: string): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'countrylist', JSON.stringify({ scc: code }));
  }

  //p_city_name
  zipCode(city_name): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'ddzipcode', JSON.stringify({ p_city_name: city_name }));
  }
}
