/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FinanceServiceService } from './finance-service.service';

describe('Service: FinanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FinanceServiceService]
    });
  });

  it('should ...', inject([FinanceServiceService], (service: FinanceServiceService) => {
    expect(service).toBeTruthy();
  }));
});
