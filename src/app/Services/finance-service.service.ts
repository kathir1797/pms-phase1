import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';

@Injectable({
  providedIn: 'root'
})
export class FinanceServiceService {

  constructor(private sf: ServiceFactoryService) { }

  addExpense(expenseObj) {
    return this.sf.callRequest('POST', 'propertyexpense', expenseObj);
  }

  addIncome(incomeObj) {
    return this.sf.callRequest('POST', 'propertyincome', incomeObj);
  }
}
