/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GetCompaniesService } from './getCompanies.service';

describe('Service: GetCompanies', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetCompaniesService]
    });
  });

  it('should ...', inject([GetCompaniesService], (service: GetCompaniesService) => {
    expect(service).toBeTruthy();
  }));
});
