/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LeaseService } from './lease.service';

describe('Service: Lease', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeaseService]
    });
  });

  it('should ...', inject([LeaseService], (service: LeaseService) => {
    expect(service).toBeTruthy();
  }));
});
