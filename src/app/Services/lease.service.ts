import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { GetManager } from '../common_models/manager_model';
import { Getlease } from '../common_models/tenant_model';

@Injectable({
  providedIn: 'root'
})
export class LeaseService {

  constructor(private sf : ServiceFactoryService) { }

  createlease(leasedet) {
    return this.sf.callRequest('POST', 'addcontract', leasedet);
  }

  terminatelease(terminateDet) {
    return this.sf.callRequest('POST', 'terminatelease', terminateDet);
  }

  getLease(det: Getlease) {
    return this.sf.callRequest('POST', 'getcontract', det);
  }

}
