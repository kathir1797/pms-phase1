import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { GetManager } from '../common_models/manager_model';
import { Observable } from 'rxjs';
import { apiResultObj } from '../common_models/company_model';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  constructor(private sf: ServiceFactoryService) { }


  addManager(manager) {
    return this.sf.callRequest('POST', 'addprtymgr', manager)
  }

  getManager(mgrdet: GetManager) : Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'getprtymgr', mgrdet)
  }

}
