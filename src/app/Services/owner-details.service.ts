import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { Observable } from 'rxjs';
import { apiResultObj } from '../common_models/company_model';

@Injectable({
  providedIn: 'root'
})
export class OwnerDetailsService {

  constructor(private sf: ServiceFactoryService) { }

  uploadw9Form(w9Details): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'addw9data', w9Details)
  }

  getw9Data(getw9): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'getw9data', getw9)
  }

  addbankDetails(bankDet): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'addbankdetails', bankDet)
  }
  getBankDetails(getBankDet): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'getbankdetails', getBankDet)
  }
}