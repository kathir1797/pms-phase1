import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { GetOwner } from '../common_models/owner';
import { apiResultObj } from '../common_models/company_model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OwnersService {

  constructor(private sf: ServiceFactoryService) { }

  addOwner(owner){
    return this.sf.callRequest('POST', 'addprtyowner', owner);
  }

  getOwner(owner : GetOwner): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'getprtyowner', owner);
  }

}
