import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { Observable } from 'rxjs';
import { apiResultObj } from '../common_models/company_model';
import { AbstractControl, FormGroup, FormGroupName } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  constructor(private sf: ServiceFactoryService) { }


  addProperty(prop) {
    return this.sf.callRequest('POST', 'addproperty', prop)
  }

  getProperty(property): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'properties', property)
  }

  propByOwner(propbyOwner) {
    return this.sf.callRequest('POST', 'searchproperties', propbyOwner)
  }

  propByAddress(propbyAdd) {
    return this.sf.callRequest('POST', 'searchproperties', propbyAdd)
  }

  parkingSpaceValidator(control: string) {
    return (formGroup: FormGroup) => {
      const parkingSpaceControl = formGroup.controls[control];
      if (!parkingSpaceControl) {
        return null;
      }

      if (parkingSpaceControl.errors && !parkingSpaceControl.errors.range) {
        return null;
      }

      if (parkingSpaceControl.value < 1 || parkingSpaceControl.value > 5) {
        parkingSpaceControl.setErrors({ space: true });
      } else {
        parkingSpaceControl.setErrors(null);
      }
    }
  }

  rangeValidator(control: string) {
    return (formGroup: FormGroup) => {
      const rangeControl = formGroup.controls[control];
      if (!rangeControl) {
        return null;
      }

      if (rangeControl.errors && !rangeControl.errors.range) {
        return null;
      }

      if (rangeControl.value < 1 || rangeControl.value > 99) {
        rangeControl.setErrors({ range: true });
      } else {
        rangeControl.setErrors(null);
      }
    }
  }

  propSizeValidator(size: string) {
    return (formGroup: FormGroup) => {
      const sizeControl = formGroup.controls[size];
      if (!sizeControl) {
        return null;
      }

      if (sizeControl.errors && !sizeControl.errors.range) {
        return null;
      }
      if (sizeControl.value < 1 || sizeControl.value > 99999) {
        sizeControl.setErrors({ sizerange: true });
      } else {
        sizeControl.setErrors(null);
      }
    }
  }

}
