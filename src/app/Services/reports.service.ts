import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { Observable } from 'rxjs';
import { apiResultObj } from '../common_models/company_model';
import { RentReportResponse } from '../common_models/reports';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private sf: ServiceFactoryService) { }

  getPropReport(proprept): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'rptproperties', proprept);
  }

  getOwnerReport(owner): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'rptowners', owner);
  }

  getTenantReport(tenant): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'rpttenant', tenant);
  }

  getLeaseReport(lease): Observable<apiResultObj>{
    return this.sf.callRequest('POST', 'rptcontract', lease);
  }

  generateMonthlyRentReport(rent): Observable<apiResultObj> {
    return this.sf.callRequest('POST', 'rptmonthlyrent', rent);
  }

}
