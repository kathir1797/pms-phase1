import { Injectable, Inject } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { isNullOrUndefined } from 'util';
@Injectable({
  providedIn: 'root'
})
export class ServiceFactoryService {

  http: any;
  envSettings: any;
  constructor(@Inject(HttpClient) http) {
    this.http = http;
    this.envSettings = environment;
  }

  callRequest(httpVerb: string, apiName: string, strifiedData?: any) {
    // header options for PUT', 'POST', and 'DELETE'
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', '*/*');
    // headers.set('Authorization', 'Bearer ' + this.session.getSession().token);
    // const dom: any = document.querySelector('body');
    // if (dom.classList.value.indexOf('custom-loading') === -1) {
    //     dom.classList.toggle('data-load');
    // }
    if (httpVerb === 'GET') {
        // const params = new URLSearchParams();
        // if (!isNullOrUndefined(strifiedData)) {
        //     const payload = JSON.parse(strifiedData);
        //     for (const key in payload) {
        //         if (payload.hasOwnProperty(key)) {
        //             params.set(key, payload[key]);
        //         }
        //     }
        // }

        // let parameterString = '';
        // if (params.toString() != null && params.toString() !== 'undefined' && params.toString() !== '') {
        //     parameterString = '?' + params.toString() + '&UTC_diff=' + new Date().getTimezoneOffset().toString();
        // }
        return this.http.get(environment.url + apiName);
    }
    if (httpVerb === 'POST') {
        // const options = { headers: headers };
        return this.http.post(environment.url + apiName, strifiedData, { headers: new HttpHeaders({
          'Content-Type': 'application/json'}) });
    }
}
}
