import { Injectable } from '@angular/core';
import { ServiceFactoryService } from './service-factory.service';
import { Observable } from 'rxjs';
import { apiResultObj } from '../common_models/company_model';

@Injectable({
  providedIn: 'root'
})
export class TenantsService {

  constructor(private sf: ServiceFactoryService) { }

  addTenant(tenant) {
    return this.sf.callRequest('POST', 'addtenant', tenant);
  }

  getTenant(tenant) : Observable<apiResultObj>{
    return this.sf.callRequest('POST', 'gettenant',tenant)
  }
}
