import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './theme/layout/admin/admin.component';
import { AuthComponent } from './theme/layout/auth/auth.component';
import { MaintenErrorComponent } from './demo/pages/maintenance/mainten-error/mainten-error.component';
const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'maintenance',
        loadChildren: () => import('./demo/pages/maintenance/maintenance.module').then(m => m.MaintenanceModule)
      },
      {
        path: 'auth',
        loadChildren: () => import('./demo/pages/authentication/authentication.module').then(m => m.AuthenticationModule)
      },
      {
        path: 'landing',
        loadChildren: () => import('./demo/pages/landing/landing.module').then(m => m.LandingModule)
      }
    ]
  },
  {
    path:'',
    pathMatch:'full',
    redirectTo:'signin'
  },
  {
    path:'default',
    component:AdminComponent,
    children:[
      {
        path: 'dashboard',
        loadChildren : () => import('../app/demo/dashboard/default/default.module').then(m => m.DefaultModule)
      },
      {
        path:'adduser',
        loadChildren : () => import('../app/demo/dashboard/add-user/add-user.module').then(m => m.AddUserModule)
      }
    ]
  },
  {
    path:'property',
    component:AdminComponent,
    children:[
      {
        path:'add',
        loadChildren : () => import('../app/demo/properties/properties.module').then(m => m.PropertiesModule)
      }
    ]
  },
  {
    path:'owner',
    component:AdminComponent,
    children:[
      {
        path:'add',
        loadChildren : () => import('../app/demo/owners/owners.module').then(m => m.OwnersModule)
      }
    ]
  },
  {
    path:'manager',
    component:AdminComponent,
    children:[
      {
        path:'add',
        loadChildren: () => import('../app/demo/dashboard/manager/manager.module').then(m => m.ManagerModule)
      }
    ]
  },
  {
    path:'tenant',
    component:AdminComponent,
    children:[
      {
        path:'add',
        loadChildren: () => import('../app/demo/tenants/tenants.module').then(m => m.TenantsModule)
      }
    ]
  },
  {
    path:'lease',
    component:AdminComponent,
    children:[
      {
        path:'',
        loadChildren: () => import('../app/demo/lease/lease.module').then(m => m.LeaseModule)
      }
    ]
  },
  {
    path:'reports',
    component:AdminComponent,
    children:[
      {
        path:'',
        loadChildren: () => import('../app/demo/reports/reports.module').then(m => m.ReportsModule)
      }
    ]
  },
  {
    path:'finance',
    component:AdminComponent,
    children:[
      {
        path:'',
        loadChildren: () => import('../app/finance/finance.module').then(m => m.FinanceModule)
      }
    ]
  },
  {
    path:'error',
    component:MaintenErrorComponent
    //loadChildren : () => import('../app/demo/pages/maintenance/mainten-error/mainten-error.module').then(m => m.MaintenErrorModule)
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
