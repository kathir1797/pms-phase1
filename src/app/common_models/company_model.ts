export class Company {

    comp_id: Number = -1;
    comp_type: Number;
    comp_name: string;
    comp_address: string;
    county: Number;
    city: Number;
    state: Number;
    zipcode: string;
    contact_person: string;
    comp_email: string;
    admin_fname: string;
    admin_lname: string;
    admin_mname: string;
    admin_pswd: string;
    active: boolean = true;
    usr_id: number;
    //usr_lname: string;
}

export class AllCompanies {
    p_comp_id: number;
    p_usr_id: number;
    p_level: number;
}

export class apiResultObj {
    success: boolean;
    errorMessage: string;
    data: any;
}

export class County{
    county_id: number;
    county_code : string;
    county_name : string;
}