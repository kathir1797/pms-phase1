export class Manager{
    p_usr_role: number
    p_usr_fname: string;
    p_usr_mname: string;
    p_usr_lname: string;
    p_usr_email: string;
    p_usr_phone: string;
    p_active: boolean;
    p_usr_id : number;
}

export class GetManager {
    p_comp_id : number;
    p_usr_id : number;
    p_manager_id : number;
}