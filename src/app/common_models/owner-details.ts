export class getW9Data {
    p_w9_id: number;
    p_comp_id: number;
    p_owner_id: number;
    p_usr_id: number;
}

export class getBankDetails {
    p_acct_id: number
    p_type: string
    p_acct_holder_id: number
    p_comp_id: number
    p_usr_id: number
}