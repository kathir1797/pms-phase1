export class Owner{
    p_owner_id : number;
    p_comp_id : number;
    p_prop_id : number;
    p_usr_fname : string;
    p_usr_mname : string;
    p_usr_lname : string;
    p_usr_email : string;
    p_usr_phone :string;
    p_usr_address : string;
    p_usr_address1 : string;
    p_usr_zipcode :string;
    p_usr_state : number;
    p_usr_city : number;
    p_active : boolean;
    p_usr_id : number;
}

export class GetOwner{
    p_comp_id: number;
    p_usr_id: number;
    p_owner_id: number;
}