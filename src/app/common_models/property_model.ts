export class AddProperty {
    p_prop_id: number
    p_comp_id: number
    p_owner_id: number
    p_prop_name: string
    p_prop_type: number
    p_prop_mode: number
    p_prop_zone: number
    p_bhk: number
    p_floor: number
    p_total_floor: number
    p_prop_status: number
    p_prop_size: number
    p_room_count: number
    p_amenities: string
    p_elect_prov: string
    p_elect_ph: string
    p_gas_prov: string
    p_gas_ph: string
    p_water_prov: string
    p_water_ph: string
    p_city: number
    p_county: number
    p_state: number
    p_parking_space: number
    p_address: string
    p_address1: string
    p_zip: string
    p_eff_date: Date
    p_end_date: Date
    p_comments: string
    p_active: boolean
    p_usr_id: number
}

export class GetProperty {
    p_comp_id : number;
    p_usr_id: number;
    p_prop_id : number;
}

export class SearchProperty {
    
}