export class GetPropertyReports {
    p_usr_id: number;
    p_comp_id: number;
}

export class PropertyReportModel {
    prop_id: number;
    comp_id: number
    prop_name: string;
    propstatus: string;
    proptype: string;
    city_name: string;
    state_name: string;
    county_name: string;
    country_name: string;
    owner_fullname: string;
    tnt_fullname: string;
    contract_name: string;
    cont_from: Date;
    cont_to: Date;
}

export interface ResDataModal {
    page: number;
    per_page: number;
    total: number;
    total_pages: number;
    data: [];
}

export class GetOwnerReports {
    p_comp_id: number;
    p_usr_id: number;
}

export class GetTenantReports {
    p_comp_id: number;
    p_usr_id: number;
    p_tnt_id: number;
}

export class GetLeaseReports {
    p_comp_id: number;
    p_usr_id: number;
}

export class MonthlyReport {
    p_comp_id: number;
    p_from_date: string;
    p_to_date: string;
    p_usr_id: number;
}

export class RentResponse {
    expense_amt: number
    income_amt: number
    net_amt: number
    owner_name: string
    period: string
    prop_id: number
    property_address: string


    constructor(prop_id, owner_name, net_amt, income_amt, expense_amt, period, property_address) {
        this.expense_amt = expense_amt;
        this.prop_id = prop_id;
        this.income_amt = income_amt;
        this.owner_name = owner_name;
        this.net_amt = net_amt;
        this.period = period;
        this.property_address = property_address;
    }
}


export class RentReportResponse {
    data: RentResponse[]
    errorMessage: string
    success: boolean
}