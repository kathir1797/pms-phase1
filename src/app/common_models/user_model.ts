export class login {
    pusr_login: string;
    pusr_paswd: string;
}

export class User {
    usr_id: Number;
    comp_id: Number;
    usr_name: string;
    active: boolean;
    usr_address: string;
    usr_address1: string;
    usr_zipcode: Number;
    state_name: string;
    city_name: string;
    user_type: string;
    pswd_reset: boolean;
}

export class ContactUs {
    p_form_id: number;
    p_comp_name: string;
    p_fname: string;
    p_lname: string;
    p_mail: string;
    p_phone: string;
}

export class ChangePassword{
    p_login_usr_id: number;
    p_usr_pswd_old: string;
    p_usr_pswd_new: string;
    p_reset_by_usr_id : number;
}

// usr_id: 1
// comp_id: 1
// usr_name: "Admin User"
// active: true
// usr_address: "Ekkaduthangal"
// usr_address1: "Chennai"
// usr_zipcode: 19
// state_name: "Alabama"
// city_name: "Cordova"
// user_type: "System Admin"