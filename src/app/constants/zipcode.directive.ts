import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[formControlName][appZipcode]'
})
export class ZipcodeDirective {

  constructor(public ng: NgControl) { }

  @HostListener('focus', ['$event'])
  onModelChange(event: any) {
    //console.log(event.target.placeholder)
    this.onInputChange(event, false);
  }

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event: any) {
    this.onInputChange(event, true);
  }

  onInputChange(event, backspace) {
    //console.log(event.target.placeholder)
    if (event.target.placeholder === 'Zip Code') {
      let newVal = event.target.value.replace(/\D/g, '');
      if (backspace && newVal.length <= 6) {
        newVal = newVal.substring(0, newVal.length - 1);
      }
      if (newVal.length === 0) {
        newVal = '';
      } else if (newVal.length <= 5) {
        newVal = newVal.replace(/^(\d{0,5})/, '$1-');
      } else if (newVal.length <= 9) {
        newVal = newVal.replace(/^(\d{0,5})(\d{0,4})/, '$1-$2');
      } else {
        newVal = newVal.substring(0, 9);
        newVal = newVal.replace(/^(\d{0,5})(\d{0,4})/, '$1-$2');
      }
      this.ng.valueAccessor.writeValue(newVal);
    }
  }

}
