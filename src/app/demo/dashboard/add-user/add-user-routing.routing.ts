import { Routes, RouterModule } from '@angular/router';
import { AddUserComponent } from './add-user.component';
import { ClientSearchComponent } from './client-search/client-search.component';

const routes: Routes = [
  {
    path: '',
    component: AddUserComponent
  },
  {
    path: 'editcompany/:id/:country/:state/:county',
    loadChildren: () => import('./add-user.module').then(m => m.AddUserModule)
    //component: AddUserComponent
  },
  {
    path: 'search',
    component: ClientSearchComponent
  }
];

export const AddUserRoutingRoutes = RouterModule.forChild(routes);
