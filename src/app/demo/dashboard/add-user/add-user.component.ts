import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';
import { GetCompaniesService } from 'src/app/Services/getCompanies.service';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { ToastrService } from 'ngx-toastr';
import { tap, map, switchMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { AllCompanies, apiResultObj, County } from 'src/app/common_models/company_model';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  // Client
  title: string;
  dddetails = new Array;
  ddlistname = new Array;
  changeDisplay: string;
  state = new Array;
  city = new Array;
  county = new Array;
  country = new Array;
  allCompanies = new AllCompanies();
  companyDetails = new Array();
  isSubmitted: boolean = false;
  showCheckbox: boolean = true;
  hidecnfmPswd: boolean;
  url: any;
  zipCode = new Array();
  maskTeleUS = [/\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/,];

  constructor(private router: Router, public ddlist: DdlistService, private gc: GetCompaniesService, private toastr: ToastrService, ) {
    this.url = this.router.url
    this.url = this.url.split('/');
    if (this.url[3] == 'editcompany') {
      this.title = 'Client Profile';
      this.changeDisplay = 'EDIT'
      this.form.get('p_active').value
    }
    else {
      this.isSubmitted = true;
      this.showCheckbox = false;
      this.title = 'Add Client';
      this.changeDisplay = 'ADD';
    }
  }

  ngOnInit() {
    var temp = this;
    this.getDDlistName("Company_Type").subscribe(response => { });
    this.getCountryDDList('CNT').subscribe(response => { })
    if (this.url[3] == 'editcompany') {
      this.getCompanies();
    }
  }

  form = new FormGroup({
    p_comp_id: new FormControl(-1, Validators.required),
    p_comp_type: new FormControl('', Validators.required),
    p_comp_name: new FormControl('', Validators.required),
    p_comp_address: new FormControl('', Validators.required),
    p_comp_address1: new FormControl(null),
    p_county: new FormControl('', Validators.required),
    p_country: new FormControl('', Validators.required),
    p_city: new FormControl('', Validators.required),
    p_state: new FormControl('', Validators.required),
    p_zipcode: new FormControl('', Validators.required),
    p_contact_person: new FormControl(null),
    p_comp_mail: new FormControl('', [Validators.required, Validators.email]),
    p_admin_fname: new FormControl('', Validators.required),
    p_admin_lname: new FormControl('', Validators.required),
    p_admin_mname: new FormControl(''),
    p_admin_pswd: new FormControl('', Validators.compose([Validators.required])),
    p_comp_tin_number: new FormControl(''),
    confirmPassword: new FormControl('', [Validators.required]),
    p_comp_phone: new FormControl('', Validators.required),
    p_active: new FormControl(true),
    p_comments: new FormControl(''),
    p_lock: new FormControl(false),
    p_usr_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).usr_id),
  }, {
    validators: this.gc.MatchPassword('p_admin_pswd', 'confirmPassword')
  });

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  toggleVisibility(e) {
    this.form.value.p_active = e.target.checked;
  }

  get f() {
    return this.form.controls;
  }



  // submit() {
  //   if (this.form.valid) {
  //     console.log(this.form.value);
  //   } else {
  //     this.toastr.error('Enter Form Details');
  //   }
  // }

  setValue() {
    this.form.setValue({ comp_name: 'Hardik Savani', comp_email: 'itsolutionstuff@gmail.com', comp_address: 'This is testing from itsolutionstuff.com' });
  }

  resetValue() {
    this.form.reset({
      p_comp_type: ''
      , p_comp_name: ''
      , p_comp_address: ''
      , p_county: ''
      , p_city: ''
      , p_state: ''
      , p_zipcode: ''
      , p_contact_person: ''
      , p_comp_mail: ''
      , p_admin_fname: '', p_admin_lname: '', p_admin_mname: '', p_admin_pswd: ''
    });
  }

  getDDlistName(listname: string) {
    return this.ddlist.getDDlist(listname)
      .pipe(
        tap(response => {
          if (response.success) {
            this.ddlistname = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  getCountryDDList(code: string) {
    return this.ddlist.countryDropDown(code).pipe(
      tap(response => {
        if (response.success) {
          this.country = response.data;
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  OnCountrySelect(val) {
    if (val != null) {
      this.getStateDDList('ST', this.country.find(x => x.country_id == val.target.value).country_code).subscribe(response => { });
    }
  }

  getStateDDList(code: string, search: string) {
    return this.ddlist.stateDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.state = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnStateSelect(val) {
    if (val != null) {
      this.getCountyDDList('CY', this.state.find(x => x.state_id == val.target.value).state_code).subscribe(response => { });
    }
  }

  getCountyDDList(code: string, search: string) {
    return this.ddlist.countyDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.county = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnCountySelect(val) {
    if (val != null) {
      this.getCityDDList('CT', this.county.find(x => x.county_id == val.target.value).county_name).subscribe(response => { });
    }
  }

  getCityDDList(code: string, search: string) {
    return this.ddlist.cityDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.city = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnCitySelect(name) {
    if (name != null) {
      this.getZipcode(name.target.value).subscribe(response => { })
    }
  }

  getZipcode(name) {
    return this.ddlist.zipCode(name)
      .pipe(
        tap(response => {
          if (response.success) {
            this.zipCode = response.data;
            this.zipCode = this.zipCode.map(x => x.zipcode)
            this.form.patchValue({ p_zipcode: this.zipCode[0] })
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  addCompany() {
    if (this.form.valid) {
      this.isSubmitted = true;
      const formValue = this.form.value
      formValue.p_comp_phone = new RegExp(this.form.get('p_comp_phone').value).source.replace(/-/g, "");
      formValue.p_zipcode = new RegExp(this.form.get('p_zipcode').value).source.replace(/-/g, "");
      this.gc.addCompany(JSON.stringify(formValue)).subscribe(response => {
        if (response.success) {
          if (this.form.controls['p_active'].value == false) {
            Swal.fire('Company', 'Deactivated Successfully');
          }
          this.toastr.success('Saved Successfully', 'Client');
          this.router.navigate(['/default/adduser/search']);
          this.form.reset();
        }
        else {
          this.isSubmitted = false;
          this.toastr.error(response.errorMessage, 'Client');
        }
      });
    } else {
      this.toastr.error('Required Field information is not available. Please enter required information in highlighted fields as below.');
      this.validateAllFormFields(this.form);
    }
    return;
  }

  getCompanies() {
    let obj = localStorage.getItem('currentuser');
    this.allCompanies.p_comp_id = this.url[4];
    this.allCompanies.p_level = 2;
    this.allCompanies.p_usr_id = JSON.parse(obj).usr_id;
    forkJoin([
      this.getDDlistName("Company_Type"),
      this.getCountryDDList('CNT'),
      this.getStateDDList('ST', this.url[5]),
      this.getCountyDDList('CY', this.url[6]),
      this.getCityDDList('CT', this.url[7])
    ]).pipe(
      switchMap(results => {
        if (results[0].success && results[1].success && results[2].success && results[3].success && results[4].success) {
          this.ddlistname = results[0].data;
          this.country = results[1].data;
          this.state = results[2].data;
          this.county = results[3].data;
          this.city = results[4].data;
        } else {
          this.toastr.warning('No data found in Dropdown');
        }
        return this.gc.getCompanies(this.allCompanies)
      })
    ).subscribe(response => {
      if (response.success) {
        let data = response.data;
        let country = this.country.filter(value => {
          if (value.country_name == data[0].country_name) {
            return value;
          }
        });

        let state = this.state.filter(value => {
          if (value.state_name == data[0].state_name) {
            return value;
          }
        });

        let type = this.ddlistname.filter(response => {
          if (response.dd_det_id == data[0].comp_type) {
            return response;
          }
        });

        let county = this.county.filter(value => {
          if (value.county_name == this.url[7]) {
            return value;
          }
        });

        let city = this.city.filter(value => { return value ? value.city_name == data[0].city_name : this.toastr.error('city not matched') })
        this.form.patchValue({ p_county: county[0].county_id }, { onlySelf: true })
        this.form.patchValue({
          p_comp_id: data[0].comp_id,
          p_comp_type: type[0].dd_det_id,
          p_comp_name: data[0].comp_name,
          p_comp_address: data[0].comp_address,
          p_country: country[0].country_id,
          p_state: state[0].state_id,
          p_city: city[0].city_name,
          p_zipcode: data[0].comp_zip,
          p_comp_mail: data[0].comp_email,
          p_admin_fname: data[0].usr_fname,
          p_admin_lname: data[0].usr_lname,
          p_admin_mname: data[0].usr_mname,
          p_admin_pswd: data[0].usr_pswd,
          confirmPassword: data[0].usr_pswd,
          p_comp_phone: data[0].comp_phone,
          p_active: data[0].active,
          p_lock: data[0].acct_lock,
          p_comp_tin_number: data[0].comp_tin_number,
          p_usr_id: JSON.parse(localStorage.getItem('currentuser')).usr_id
        }, { onlySelf: true });

        if (data[0].acct_lock == true) {
          this.form.get('p_lock').value
        }


      } else {
        this.toastr.error(response.errorMessage, 'Client');
      }
    });
  }
}