import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddUserComponent } from './add-user.component';
import { AddUserRoutingRoutes } from './add-user-routing.routing';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { CustomFormsModule } from 'ngx-custom-validators';
import { ClientSearchComponent } from './client-search/client-search.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { TextMaskModule } from 'angular2-text-mask';
/*import {CurrencyMaskModule} from 'ng2-currency-mask';*/
import { NgNumberFormatterModule } from 'ng-number-formatter';
import { ZipcodeDirective } from 'src/app/constants/zipcode.directive';

@NgModule({
  imports: [
    CommonModule,
    AddUserRoutingRoutes,
    SharedModule,
    CustomFormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    TypeaheadModule.forRoot(),
    TextMaskModule,
    /*CurrencyMaskModule,*/
    NgNumberFormatterModule,
  ],
  declarations: [AddUserComponent, ClientSearchComponent]
})
export class AddUserModule { }
