import { Component, OnInit, ViewChild } from '@angular/core';
import { AllCompanies } from 'src/app/common_models/company_model';
import { Router } from '@angular/router';
import { GetCompaniesService } from 'src/app/Services/getCompanies.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-client-search',
  templateUrl: './client-search.component.html',
  styleUrls: ['./client-search.component.css']
})
export class ClientSearchComponent implements OnInit {

  constructor(private router: Router, private gc: GetCompaniesService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getCompanies();
  }

  // @ViewChild('basicModal') popup : any;

  allCompanies = new AllCompanies();
  companyDetails = new Array;
  searchText : string;
  p:number = 1;
  totalRecords: number;
  showModal: boolean;

  getCompanies(){
    let obj = localStorage.getItem('currentuser');
    this.allCompanies.p_comp_id =JSON.parse(obj).comp_id;
    this.allCompanies.p_level = 2;
    this.allCompanies.p_usr_id = JSON.parse(obj).usr_id;
    this.gc.getCompanies(this.allCompanies).subscribe(response => {
      if(response.success){
        this.companyDetails = response.data;
        this.totalRecords = response.data.length;
      } else {
        this.toastr.error(response.errorMessage, 'Company');
      }
    });
  }

  editcompany(company: any){
    this.router.navigate(['/default/adduser/editcompany', company.comp_id, company.country_code, company.state_code, company.county_name]);
  }
}
//state_code