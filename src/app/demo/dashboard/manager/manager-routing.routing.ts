import { Routes, RouterModule } from '@angular/router';
import { ManagerComponent } from './manager.component';
import { ViewComponent } from './view/view.component';

const routes: Routes = [ {
  path:'',
  component:ManagerComponent
  },{
    path:'view',
    component: ViewComponent
  },
{
  path : 'editmanager/:obj',
  loadChildren : () => import('./manager.module').then(m => m.ManagerModule)
}];

export const ManagerRoutingRoutes = RouterModule.forChild(routes);
