import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormControlName, FormControlDirective } from '@angular/forms';
import { Router } from '@angular/router';
import { ManagerService } from 'src/app/Services/manager.service';
import { ToastrService } from 'ngx-toastr';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { GetManager } from 'src/app/common_models/manager_model';
import { tap, switchMap } from 'rxjs/operators';
import { GetCompaniesService } from 'src/app/Services/getCompanies.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

  ddlistname = new Array;
  mgrdetails = new GetManager();
  details = new Array;
  url: any;
  currentuser = localStorage.getItem('currentuser');
  isSubmitted : boolean;
  isShowCheckBox: boolean = false;
  title: string = "User Creation";

  constructor(private router: Router, private mg: ManagerService, private ddlist: DdlistService, private toastr: ToastrService, private fc: GetCompaniesService) {
    this.url = this.router.url;
    this.url = this.url.split('/');
    console.log(this.url)
    let currentuser = localStorage.getItem('currentuser');
  }

  ngOnInit() {
    this.getDDlistName('User_Type').subscribe(response => { });
    if (this.url[3] == 'editmanager') {
      this.title = 'User Profile'
      this.viewManagers();
      this.isShowCheckBox = true;
      this.form.get('p_active').value
    } else {
      this.isSubmitted = true;
      this.isShowCheckBox = false;
    }
  }

  form = new FormGroup({
    p_manager_id: new FormControl(-1),
    p_comp_id: new FormControl(JSON.parse(this.currentuser).comp_id),
    p_role: new FormControl('', Validators.required),
    p_usr_fname: new FormControl('', Validators.required),
    p_usr_mname: new FormControl(''),
    p_usr_lname: new FormControl('', Validators.required),
    p_usr_email: new FormControl('', [Validators.required, Validators.email]),
    p_pswd: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', [Validators.required]),
    p_usr_phone: new FormControl(1234567890),
    p_active: new FormControl(true),
    p_comments: new FormControl(''),
    p_lock: new FormControl(false),
    p_usr_id: new FormControl(JSON.parse(this.currentuser).usr_id)
  }, {
    validators: this.fc.MatchPassword('p_pswd', 'confirmPassword')
  });

  get f() {
    return this.form.controls;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  submit() {
    if (this.form.status === 'VALID') {
      console.log(this.form.value);
      // this.toastr.show('enter valid details');
    }
  }

  getDDlistName(listname: string) {
    return this.ddlist.getDDlist(listname)
      .pipe(
        /* use tap() to set `this.ddlistname` */
        tap(response => {
          if (response.success) {
            this.ddlistname = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  addPropertyManager() {
    if (this.form.valid) {
      this.mg.addManager(JSON.stringify(this.form.value)).subscribe(response => {
        if (response.success) {
          if (this.form.controls['p_active'].value == false) {
            Swal.fire('Property Manager', 'Deactivated Successfully');
          }
          this.toastr.success('Saved Successfully', 'User');
          this.form.reset();
          this.router.navigate(['/manager/add/view']);
        }
        else {
          this.toastr.error(response.errorMessage, 'User');
        }
      });
    } else {
      this.validateAllFormFields(this.form);
      this.toastr.error('Required Field information is not available. Please enter required information in highlighted fields');
    }
  }

  viewManagers() {
    this.mgrdetails.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.mgrdetails.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.mgrdetails.p_manager_id = parseInt(this.url[4]);
    this.getDDlistName('User_Type').pipe(
      switchMap(result => {
        if (result.success) {
          this.ddlistname = result.data;
        }
        return this.mg.getManager(this.mgrdetails)
      })
    ).subscribe(response => {
      if (response.success) {
        let data = response.data;
        let filteredData = this.ddlistname.filter(value => {
          if (value.dd_value == data[0].user_type) {
            return value;
          }
        });
        this.form.patchValue({
          p_manager_id: data[0].manager_id,
          p_role: filteredData[0].dd_det_id,
          p_comp_id: data[0].comp_id,
          p_usr_fname: data[0].manager_fname,
          p_usr_mname: data[0].manager_mname,
          p_usr_lname: data[0].manager_lname,
          p_usr_email: data[0].manager_mail,
          p_pswd: data[0].usr_pswd,
          confirmPassword: data[0].usr_pswd,
          p_active: data[0].active,
          p_usr_id: JSON.parse(this.currentuser).usr_id
        })
      } else {
        this.toastr.error(response.errorMessage);
      }
    });
  }

  // viewManagers() {
  //   this.mgrdetails.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
  //   this.mgrdetails.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
  //   this.mgrdetails.p_manager_id = parseInt(this.url[4]);
  //   this.getDDlistName('User_Type').subscribe(response => { console.log(response) });
  //   this.mg.getManager(this.mgrdetails).subscribe(response => {
  //     if (response.success) {
  //       let data = response.data;
  //       // let data = response.data.filter(value1 => {
  //       //   if (parseInt(value1.manager_id) == parseInt(this.url[4])) {
  //       //     return value1;
  //       //   }
  //       // });

  //       // let filteredData = this.ddlistname.filter(value => {
  //       //   if (value.dd_value == data[0].user_type) {
  //       //     return value;
  //       //   }
  //       // });

  //       this.form.patchValue({
  //         p_manager_id: new FormControl(-1),
  //         p_comp_id: new FormControl(JSON.parse(this.currentuser).comp_id),
  //         p_role: new FormControl('', Validators.required),
  //         p_usr_fname: new FormControl('', Validators.required),
  //         p_usr_mname: new FormControl(''),
  //         p_usr_lname: new FormControl('', Validators.required),
  //         p_usr_email: new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
  //         p_pswd: new FormControl('', Validators.required),
  //         p_usr_phone: new FormControl(1234567890),
  //         p_active: new FormControl(true),
  //         p_usr_id: new FormControl(JSON.parse(this.currentuser).usr_id)
  //       })

  //       // this.form.get('p_pswd').setValue(data[0].usr_pswd);
  //       // this.form.get('p_manager_id').setValue(data[0].manager_id);
  //       // this.form.get('p_comp_id').setValue(data[0].comp_id);
  //       // this.form.get('p_usr_fname').setValue(data[0].manager_fname);
  //       // this.form.get('p_usr_mname').setValue(data[0].manager_mname);
  //       // this.form.get('p_usr_lname').setValue(data[0].manager_lname);
  //       // this.form.get('p_usr_email').setValue(data[0].manager_mail);
  //       // this.form.get('p_usr_phone').setValue(data[0].manager_phone);
  //       // this.form.get('p_active').setValue(data[0].active);
  //       // this.form.patchValue({ p_role: filteredData[0].dd_det_id }, { onlySelf: true });
  //     } else {
  //       this.toastr.error(response.errorMessage, 'User')
  //     }
  //   })
  // }

}
