import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerComponent } from './manager.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { ManagerRoutingRoutes } from './manager-routing.routing';
import { ViewComponent } from './view/view.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    ManagerRoutingRoutes,
    SharedModule,
    Ng2SearchPipeModule,
    NgxPaginationModule
  ],
  declarations: [ManagerComponent, ViewComponent]
})
export class ManagerModule { }
