import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GetManager } from 'src/app/common_models/manager_model';
import { ManagerService } from 'src/app/Services/manager.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  constructor(private router: Router, private ms : ManagerService, private toastr : ToastrService) { }

  public mgrdetails = new GetManager();
  details = new Array;
  searchText : any;
  p:number = 1;
  totalRecords: number;

  ngOnInit() {
    this.viewManagers();
  }

  editManager(element){
    this.router.navigate(['/manager/add/editmanager', element.manager_id]);
    console.log(element);
  }

  viewManagers() {
    this.mgrdetails.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.mgrdetails.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.mgrdetails.p_manager_id = 1;
    this.ms.getManager(this.mgrdetails).subscribe(response => {
      if(response.success) {
        this.details =  response.data;
        this.totalRecords = response.data.length;
      }  else {
        this.toastr.error(response.errorMessage, 'User');
      }
    })
  }
 
}
