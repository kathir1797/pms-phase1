import { Routes, RouterModule } from '@angular/router';
import { LeaseComponent } from './lease.component';
import { TerminateLeaseComponent } from './terminate-lease/terminate-lease.component';
import { LeaseSearchComponent } from './lease-search/lease-search.component';

const routes: Routes = [
  {
    path: '',
    component: LeaseComponent
  },
  {
    path : 'terminate',
    component: TerminateLeaseComponent
  },
  {
    path : 'terminate/:name',
    component : TerminateLeaseComponent
  },
  {
    path: 'search',
    component: LeaseSearchComponent
  }
];

export const LeaseRoutingRoutes = RouterModule.forChild(routes);
