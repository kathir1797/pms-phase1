import { Component, OnInit } from '@angular/core';
import { LeaseService } from 'src/app/Services/lease.service';
import { ToastrService } from 'ngx-toastr';
import { GetManager } from 'src/app/common_models/manager_model';
import { Router } from '@angular/router';
import { Getlease } from 'src/app/common_models/tenant_model';

@Component({
  selector: 'app-lease-search',
  templateUrl: './lease-search.component.html',
  styleUrls: ['./lease-search.component.css']
})
export class LeaseSearchComponent implements OnInit {

  searchText: string;
  getlease = new Getlease();
  leaseDetails = new Array;
  totalRecords: number;
  p: number = 1;

  constructor(private ls: LeaseService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.getLease();
  }

  getLease() {
    this.getlease.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getlease.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.getlease.p_tenant_cont_id = null;
    this.ls.getLease(this.getlease).subscribe(response => {
      if (response.success) {
        this.leaseDetails = response.data;
        this.totalRecords = response.data.length;
      } else {
        this.toastr.error(response.errorMessage);
      }
    });
  }

  terminateLease(lease) {
    this.router.navigate(['/lease/terminate', lease.tenant_cont_id]);
  }

}
