import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { GetProperty } from 'src/app/common_models/property_model';
import { PropertyService } from 'src/app/Services/property.service';
import { ToastrService } from 'ngx-toastr';
import { GetTenant } from 'src/app/common_models/tenant_model';
import { TenantsService } from 'src/app/Services/tenants.service';
import { FormControl, FormGroup, FormControlName, Validators } from '@angular/forms';
import { LeaseService } from 'src/app/Services/lease.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { BsDatepickerConfig, BsDatepickerDirective } from 'ngx-bootstrap/datepicker/';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lease',
  templateUrl: './lease.component.html',
  styleUrls: ['./lease.component.css']
})
export class LeaseComponent implements OnInit {

  public getprop = new GetProperty();
  public tenantList = new GetTenant();
  tenant = new Array();
  propdet = new Array();
  isShowCheckBox: boolean = false;
  bsInlineValue = new Date();
  todayDate: Date;
  datepickerConfig: Partial<BsDatepickerConfig>;
  // { dateInputFormat: 'MM-DD-YYYY', isAnimated: true }

  constructor(private datePipe: DatePipe, private router: Router, private cs: PropertyService, private toastr: ToastrService, private ts: TenantsService, private ls: LeaseService) {
    this.datepickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      dateInputFormat: 'MM-DD-YYYY',
      isAnimated: true,
      showWeekNumbers: false
    });
  }
  @ViewChild(BsDatepickerDirective, { static: false }) datepicker: BsDatepickerDirective;

  @HostListener('window:scroll')
  onScrollEvent() {
    this.datepicker.hide();
  }

  ngOnInit() {
    this.todayDate = new Date();
    this.getTenant();
    this.getProperty();
  }

  leaseform = new FormGroup({
    p_tenant_cont_id: new FormControl(-1),
    p_tenant_id: new FormControl('', Validators.required),
    p_contract_name: new FormControl(''),
    p_prop_id: new FormControl('', Validators.required),
    p_cont_from: new FormControl(null, Validators.required),
    p_cont_to: new FormControl(null, Validators.required),
    p_rent_amt: new FormControl('', Validators.required),
    p_active: new FormControl(true),
    p_comments: new FormControl(''),
    p_usr_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).usr_id)
  });

  get f() {
    return this.leaseform.controls;
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }


  createlease() {
    console.log(this.leaseform.value)
    if (this.leaseform.valid) {
      const formValue = this.leaseform.value;
      formValue.p_cont_from = this.datePipe.transform(formValue.p_cont_from, 'MM-dd-yyyy');
      formValue.p_cont_to = this.datePipe.transform(formValue.p_cont_to, 'MM-dd-yyyy');
      this.ls.createlease(JSON.stringify(formValue)).subscribe(response => {
        if (response.success) {
          if (this.leaseform.controls['p_active'].value == false) {
            Swal.fire('Lease', 'Deactivated Successfully');
          }
          this.toastr.success('Saved Successfully', 'Lease');
          this.leaseform.reset();
          this.router.navigate(['/lease/search']);
        } else {
          this.toastr.error(response.errorMessage, 'Lease');
        }
      });
    } else {
      this.validateAllFormFields(this.leaseform);
      this.toastr.error('Required Field information is not available. Please enter required information in highlighted fields as below.', 'Lease');
    }
  }



  getProperty() {
    this.getprop.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getprop.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.cs.getProperty(this.getprop).subscribe(response => {
      if (response.success) {
        this.propdet = response.data;
      } else {
        this.toastr.info('No Data', 'Property')
      }
    });
  }

  getTenant() {
    this.tenantList.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.tenantList.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.ts.getTenant(this.tenantList).subscribe(response => {
      if (response.success) {
        console.log(response.data);
        this.tenant = response.data;
      } else {
        this.toastr.info('No Data Found', 'Tenant');
      }
    });
  }

}
