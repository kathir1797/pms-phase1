import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { LeaseComponent } from './lease.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { LeaseRoutingRoutes } from './lease-routing.routing';
import { TerminateLeaseComponent } from './terminate-lease/terminate-lease.component';
import { LeaseSearchComponent } from './lease-search/lease-search.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LeaseRoutingRoutes,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    NgbDatepickerModule,
    BsDatepickerModule,
  ],
  declarations: [LeaseComponent, TerminateLeaseComponent, LeaseSearchComponent],
  providers :[DatePipe]
})
export class LeaseModule { }
