import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { LeaseService } from 'src/app/Services/lease.service';
import { Router } from '@angular/router';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { GetManager } from 'src/app/common_models/manager_model';
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs/operators';
import { Getlease } from 'src/app/common_models/tenant_model';
import { DatePipe } from '@angular/common';
import { BsDatepickerConfig, BsDatepickerDirective } from 'ngx-bootstrap/datepicker/';

@Component({
  selector: 'app-terminate-lease',
  templateUrl: './terminate-lease.component.html',
  styleUrls: ['./terminate-lease.component.css']
})
export class TerminateLeaseComponent implements OnInit {

  leaseDetails = new Array;
  reason = new Array;
  leaseModel = new Getlease();
  url: any;
  datepickerConfig: Partial<BsDatepickerConfig>;

  constructor(private datePipe: DatePipe, private ls: LeaseService, private router: Router, private ddlist: DdlistService, private toastr: ToastrService) {
    this.datepickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      dateInputFormat: 'MM-DD-YYYY',
      isAnimated: true,
      showWeekNumbers: false
    });
  }
  @ViewChild(BsDatepickerDirective, { static: false }) datepicker: BsDatepickerDirective;

  @HostListener('window:scroll')
  onScrollEvent() {
    this.datepicker.hide();
  }

  ngOnInit() {
    this.getReason('Terminate_Rsn').subscribe(response => { });
    this.url = this.router.url;
    this.url = this.url.split('/');
    console.log(this.url);
    if (this.url[2] == 'terminate') {
      this.getLease();
    }
  }

  terminateform = new FormGroup({
    p_tnt_cont_id: new FormControl('', Validators.required),
    p_rsn_id: new FormControl('', Validators.required),
    p_rsn_cmts: new FormControl('', Validators.required),
    p_trm_date: new FormControl(null, Validators.required),
    p_usr_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).usr_id)
  });

  get f() {
    return this.terminateform.controls;
  }

  getReason(code: string) {
    return this.ddlist.getDDlist(code).pipe(
      tap(response => {
        if (response.success) {
          this.reason = response.data;
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  getLease() {
    this.leaseModel.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.leaseModel.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.leaseModel.p_tenant_cont_id = parseInt(this.url[3]);
    this.ls.getLease(this.leaseModel).subscribe(response => {
      if (response.success) {
        this.leaseDetails = response.data;
        let start = this.datePipe.transform(this.leaseDetails[0].cont_from, 'MM-dd-yyyy');
        let end = this.datePipe.transform(this.leaseDetails[0].cont_to, 'MM-dd-yyyy');
        this.datepickerConfig = Object.assign({}, {
        containerClass: 'theme-dark-blue',
        dateInputFormat: 'MM-DD-YYYY',
        isAnimated: true,
        showWeekNumbers: false,
        minDate: new Date(start),
        maxDate: new Date(end)
      });
        this.terminateform.patchValue({ p_tnt_cont_id: this.leaseDetails[0].prop_id });
      } else {
        this.toastr.error(response.errorMessage, 'Lease');
      }
    });
  }

  terminateLease() {
    if (this.terminateform.valid) {
      const formValue = this.terminateform.value;
      formValue.p_trm_date = this.datePipe.transform(formValue.p_trm_date, 'MM-dd-yyyy');
      this.ls.terminatelease(JSON.stringify(formValue)).subscribe(response => {
        if (response.success) {
          this.toastr.success('Lease Terminated', 'Lease');
          this.router.navigate(['/lease/search']);
        } else {
          this.toastr.error(response.errorMessage, 'Lease');
        }
      });
    } else {
      this.toastr.error('Required fields are not available, Please enter the valid information as below', 'Lease');
    }
  }
  // cont_from: "2020-04-22T00:00:00"
  // cont_to: "2020-05-02T00:00:00"
}
