import { Component, OnInit } from '@angular/core';
import { Owner, GetOwner } from 'src/app/common_models/owner';
import { FormGroup, FormControl, FormControlName, Validators } from '@angular/forms';
import { BsDatepickerConfig, BsDatepickerDirective, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker/';
import { OwnersService } from 'src/app/Services/owners.service';
import { ToastrService } from 'ngx-toastr';
import { GetProperty } from 'src/app/common_models/property_model';
import { PropertyService } from 'src/app/Services/property.service';
import { Router } from '@angular/router';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { tap, switchMap, count } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { forkJoin } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-owner',
  templateUrl: './add-owner.component.html',
  styleUrls: ['./add-owner.component.css']
})
export class AddOwnerComponent implements OnInit {

  ownerData = new Owner;
  obj = localStorage.getItem('currentuser');
  public getprop = new GetProperty();
  getownerreq = new GetOwner();
  propdet = new Array;
  ownerdet = new Array;
  ddlistname = new Array;
  state = new Array;
  city = new Array;
  county = new Array;
  country = new Array;
  url: any;
  isSubmitted: boolean;
  isShowCheckBox: boolean = false;
  datepickerConfig: Partial<BsDatepickerConfig>;
  minMode: BsDatepickerViewMode = 'year';
  public maskTeleUS = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskTIN = ['9', /\d/, /\d/, '-', '7', /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  zipCode = new Array;


  constructor(private datePipe: DatePipe, private os: OwnersService, private toastr: ToastrService, private cs: PropertyService, private router: Router, public ddlist: DdlistService) {
    this.datepickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      minMode: this.minMode,
      dateInputFormat: 'YYYY'
    });
  }

  ngOnInit() {
    this.getProperty().subscribe(response => { });
    this.getDDlistName("Company_Type").subscribe(response => { });
    this.getCountryDDList('CNT').subscribe(response => { });
    this.url = this.router.url;
    this.url = this.url.split('/')
    if (this.url[3] == 'editowner') {
      this.getOwner();
      this.isShowCheckBox = true;
      this.form.get('p_active').value
    } else {
      this.isSubmitted = true;
      this.isShowCheckBox = false;
    }
  }

  form = new FormGroup({
    p_owner_id: new FormControl(-1),
    p_comp_id: new FormControl(JSON.parse(this.obj).comp_id, Validators.required),
    p_prop_id: new FormControl('', Validators.required),
    p_usr_fname: new FormControl('', Validators.required),
    p_usr_mname: new FormControl(''),
    p_usr_lname: new FormControl('', Validators.required),
    p_usr_email: new FormControl('', [Validators.required, Validators.email]),
    p_usr_phone: new FormControl('', Validators.required),
    p_country: new FormControl('', Validators.required),
    p_state: new FormControl('', Validators.required),
    p_city: new FormControl('', Validators.required),
    p_county: new FormControl('', Validators.required),
    p_zip: new FormControl('', Validators.required),
    p_address: new FormControl('', Validators.required),
    p_comments: new FormControl(''),
    p_active: new FormControl(true),
    p_usr_id: new FormControl(JSON.parse(this.obj).usr_id, Validators.required)
  });

  get f() {
    return this.form.controls;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  getDDlistName(listname: string) {
    return this.ddlist.getDDlist(listname)
      .pipe(
        /* use tap() to set `this.ddlistname` */
        tap(response => {
          if (response.success) {
            this.ddlistname = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  getCountryDDList(code: string) {
    return this.ddlist.countryDropDown(code).pipe(
      tap(response => {
        if (response.success) {
          this.country = response.data;
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  OnCountrySelect(val) {
    if (val != null) {
      this.getStateDDList('ST', this.country.find(x => x.country_id == val.target.value).country_code).subscribe(response => { console.log(response) });
    }
  }

  getStateDDList(code: string, search: string) {
    return this.ddlist.stateDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.state = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnStateSelect(val) {
    if (val != null) {
      this.getCountyDDList('CY', this.state.find(x => x.state_id == val.target.value).state_code).subscribe(response => { console.log(response) });
    }
  }

  getCountyDDList(code: string, search: string) {
    return this.ddlist.countyDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.county = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnCountySelect(val) {
    if (val != null) {
      this.getCityDDList('CT', this.county.find(x => x.county_id == val.target.value).county_name).subscribe(response => { console.log(response) });
    }
  }

  getCityDDList(code: string, search: string) {
    return this.ddlist.cityDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.city = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnCitySelect(name) {
    if (name.target.value != null) {
      this.getZipcode(name.target.value).subscribe(response => { })
    }
  }

  getZipcode(name) {
    return this.ddlist.zipCode(name)
      .pipe(
        tap(response => {
          if (response.success) {
            this.zipCode = response.data;
            this.zipCode = this.zipCode.map(x => x.zipcode)
            this.form.patchValue({ p_zip: this.zipCode[0] })
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  addOwner() {
    if (this.form.valid) {
      const formValue = this.form.value;
      formValue.p_usr_phone = new RegExp(this.form.get('p_usr_phone').value).source.replace(/-/g, "");
      //formValue.p_w9_year = this.datePipe.transform(formValue.p_w9_year, 'yyyy');
      //formValue.p_w9_tin_number = new RegExp(this.form.get('p_w9_tin_number').value).source.replace(/-/g, "");
      this.os.addOwner(formValue).subscribe(response => {
        if (response.success) {
          if (this.form.controls['p_active'].value == false) {
            Swal.fire('Owner', 'Deactivated Successfully');
          }
          this.toastr.success('Saved Successfully', 'Owner');
          this.form.reset();
          this.router.navigate(['/owner/add']);
        }
        else {
          this.toastr.error(response.errorMessage, 'Owner');
        }
      });
    } else {
      this.validateAllFormFields(this.form);
      this.toastr.error('Required Field information is not available. Please enter required information in highlighted fields');
    }
  }

  getProperty() {
    this.getprop.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getprop.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.getprop.p_prop_id = null;
    return this.cs.getProperty(this.getprop).pipe(
      tap(response => {
        if (response.success) {
          console.log(response);
          this.propdet = response.data;
        } else {
          this.toastr.info(response.errorMessage, 'Property')
        }
      })
    );
  }

  getOwner() {
    this.getownerreq.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getownerreq.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.getownerreq.p_owner_id = this.url[4];
    forkJoin([
      this.getDDlistName("Company_Type"),
      this.getCountryDDList('CNT'),
      this.getStateDDList('ST', this.url[5]),
      this.getCountyDDList('CY', this.url[6]),
      this.getCityDDList('CT', this.url[7])
    ]).pipe(
      switchMap(results => {
        if (results[0].success && results[1].success && results[2].success && results[3].success && results[4].success) {
          this.ddlistname = results[0].data;
          this.country = results[1].data;
          this.state = results[2].data;
          this.county = results[3].data;
          this.city = results[4].data
        } else {
          this.toastr.warning('No data found in Dropdown', 'Owner');
        }
        return this.os.getOwner(this.getownerreq)
      })
    ).subscribe(response => {
      if (response.success) {
        let data = response.data;
        let country = this.country.filter(value => {
          if (value.country_name == data[0].country_name) {
            return value;
          }
        });

        let state = this.state.filter(value => {
          if (value.state_name == data[0].state_name) {
            return value;
          }
        });

        let type = this.ddlistname.filter(response => {
          if (response.dd_value == data[0].w9_tx_type) {
            return response;
          }
        });

        let county = this.county.filter(value => {
          if (value.county_name == data[0].county_name) {
            return value;
          }
        });

        let city = this.city.filter(value => { return value ? value.city_name == data[0].city_name : this.toastr.error('city not matched') })

        console.log(county, state, country, data, type, data[0].w9_city_name);

        if (data[0].prop_id != 0) {
          this.form.get('p_prop_id').patchValue(data[0].prop_id);
        }
        this.form.patchValue({ p_county: county[0].county_id }, { emitEvent: false })
        this.form.patchValue({
          p_owner_id: data[0].owner_id,
          p_comp_id: data[0].comp_id,
          p_usr_fname: data[0].owner_fname,
          p_usr_mname: data[0].owner_mname,
          p_usr_lname: data[0].owner_lname,
          p_usr_email: data[0].owner_mail,
          p_usr_phone: data[0].owner_phone,
          p_country: country[0].country_id,
          p_state: state[0].state_id,
          p_city: city[0].city_name,
          p_address: data[0].owner_address,
          p_zip: data[0].owner_zip,
          p_active: data[0].active,
          p_usr_id: JSON.parse(this.obj).usr_id
        })
        // p_owner_id: new FormControl(-1),
        // p_comp_id: new FormControl(JSON.parse(this.obj).comp_id, Validators.required),
        // p_prop_id: new FormControl('', Validators.required),
        // p_usr_fname: new FormControl('', Validators.required),
        // p_usr_mname: new FormControl(''),
        // p_usr_lname: new FormControl('', Validators.required),
        // p_usr_email: new FormControl('', [Validators.required, Validators.email]),
        // p_usr_phone: new FormControl('', Validators.required),
        // p_country: new FormControl('', Validators.required),
        // p_state: new FormControl('', Validators.required),
        // p_city: new FormControl('', Validators.required),
        // p_county: new FormControl('', Validators.required),
        // p_zip: new FormControl('', Validators.required),
        // p_address: new FormControl('', Validators.required),
        // p_comments: new FormControl('', Validators.required),
        // p_active: new FormControl(true),
        // p_usr_id: new FormControl(JSON.parse(this.obj).usr_id, Validators.required)
        // this.form.get('p_owner_id').setValue(data[0].owner_id);
        // this.form.get('p_comp_id').setValue(data[0].comp_id);
        // this.form.get('p_usr_fname').setValue(data[0].owner_fname);
        // this.form.get('p_usr_mname').setValue(data[0].owner_mname);
        // this.form.get('p_usr_lname').setValue(data[0].owner_lname);
        // this.form.get('p_usr_email').setValue(data[0].owner_mail);
        // this.form.get('p_usr_phone').setValue(data[0].owner_phone);
        // this.form.get('p_active').setValue(data[0].active);
        // this.form.get('p_usr_id').setValue(JSON.parse(this.obj).usr_id);
      } else {
        this.toastr.error(response.errorMessage, 'Owner')
      }
    });
  }

  // getOwner() {
  //   this.getownerreq.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
  //   this.getownerreq.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
  //   this.getownerreq.p_owner_id = this.url[4];
  //   this.os.getOwner(this.getownerreq).subscribe(response => {
  //     if (response.success) {
  //       let data = response.data.filter(value => {
  //         if (parseInt(value.owner_id) == parseInt(this.url[4])) {
  //           return value;
  //         }
  //       });
  //       if (data[0].prop_id != 0) {
  //         this.form.get('p_prop_id').setValue(data[0].prop_id);
  //       }
  //       this.form.get('p_owner_id').setValue(data[0].owner_id);
  //       this.form.get('p_comp_id').setValue(data[0].comp_id);
  //       this.form.get('p_usr_fname').setValue(data[0].owner_fname);
  //       this.form.get('p_usr_mname').setValue(data[0].owner_mname);
  //       this.form.get('p_usr_lname').setValue(data[0].owner_lname);
  //       this.form.get('p_usr_email').setValue(data[0].owner_mail);
  //       this.form.get('p_usr_phone').setValue(data[0].owner_phone);
  //       this.form.get('p_active').setValue(data[0].active);
  //       this.form.get('p_usr_id').setValue(JSON.parse(this.obj).usr_id);
  //     } else {
  //       this.toastr.info(response.errorMessage, 'Owner Result')
  //     }
  //   });
  // }
}
