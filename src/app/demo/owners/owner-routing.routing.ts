import { Routes, RouterModule } from '@angular/router';
import { OwnersComponent } from './owners.component';
import { AddOwnerComponent } from './add-owner/add-owner.component';
import { W9UploadComponent } from './w9-upload/w9-upload.component';

const routes: Routes = [
  { 
    path :'',
    component : OwnersComponent
   },
   {
     path : 'addowner',
     component : AddOwnerComponent
   },{
     path : 'editowner/:id/:country/:state/:county',
     component : AddOwnerComponent
   },
   {
     path: 'uploadw9/:id',
     component: W9UploadComponent
   }
];

export const OwnerRoutingRoutes = RouterModule.forChild(routes);
