import { Component, OnInit } from '@angular/core';
import { OwnersService } from 'src/app/Services/owners.service';
import { ToastrService } from 'ngx-toastr';
import { GetOwner } from 'src/app/common_models/owner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-owners',
  templateUrl: './owners.component.html',
  styleUrls: ['./owners.component.css']
})
export class OwnersComponent implements OnInit {

  ownerdet = new Array;
  getownerreq = new GetOwner();
  searchText: any;
  p: number = 1;
  totalRecords: number;
  isOpen = false;

  constructor(private os: OwnersService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.getOwner();
  }

  getOwner() {
    this.getownerreq.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getownerreq.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.getownerreq.p_owner_id = null;
    this.os.getOwner(this.getownerreq).subscribe(response => {
      if (response.success) {
        this.ownerdet = response.data
        this.totalRecords = response.data.length;
      } else {
        this.toastr.info(response.errorMessage, 'Owner');
      }
    })
  }

  editOwner(element) {
    console.log(element);
    this.router.navigate(['/owner/add/editowner', element.owner_id, element.country_code, element.state_code, element.county_name]);
  }

  openTab() {
    this.isOpen = !this.isOpen;
    console.log(this.isOpen);
    // var i, x;
    // x = document.getElementsByClassName("containerTab");
    // for (i = 0; i < x.length; i++) {
    //   x[i].style.display = "none";
    // }
    // document.getElementById(tabName).style.display = "block";
  }
}
///country_code  state_code  w9_county_name
