import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { OwnersComponent } from './owners.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { OwnerRoutingRoutes } from './owner-routing.routing';
import { AddOwnerComponent } from './add-owner/add-owner.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker/';
import { TextMaskModule } from 'angular2-text-mask';
/*import {CurrencyMaskModule} from 'ng2-currency-mask';*/
import { NgNumberFormatterModule } from 'ng-number-formatter';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead/';
import { W9UploadComponent } from './w9-upload/w9-upload.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OwnerRoutingRoutes,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    NgbModule,
    BsDatepickerModule,
    TextMaskModule,
    /*CurrencyMaskModule,*/
    NgNumberFormatterModule, 
    TypeaheadModule.forRoot(),
    AccordionModule.forRoot(),
    TabsModule.forRoot()
  ],
  declarations: [OwnersComponent, AddOwnerComponent, W9UploadComponent], providers: [DatePipe]
})
export class OwnersModule { }
