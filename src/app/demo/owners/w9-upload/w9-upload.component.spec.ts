/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { W9UploadComponent } from './w9-upload.component';

describe('W9UploadComponent', () => {
  let component: W9UploadComponent;
  let fixture: ComponentFixture<W9UploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ W9UploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(W9UploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
