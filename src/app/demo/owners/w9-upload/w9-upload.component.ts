import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BsDatepickerConfig, BsDatepickerDirective, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker/';
import { Router } from '@angular/router';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { ToastrService } from 'ngx-toastr';
import { tap, switchMap, count } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OwnerDetailsService } from 'src/app/Services/owner-details.service';
import { DatePipe } from '@angular/common';
import { getW9Data, getBankDetails } from 'src/app/common_models/owner-details';
@Component({
  selector: 'app-w9-upload',
  templateUrl: './w9-upload.component.html',
  styleUrls: ['./w9-upload.component.css']
})
export class W9UploadComponent implements OnInit {

  isSubmitted: boolean;
  showW9: boolean;
  showBankModal: boolean;
  isShowCheckBox: boolean = false;
  datepickerConfig: Partial<BsDatepickerConfig>;
  minMode: BsDatepickerViewMode = 'year';
  url: any;
  ddlistname = new Array;
  accDDType = new Array;
  state = new Array;
  city = new Array;
  county = new Array;
  country = new Array;
  zipCode = new Array;
  w9details = new Array;
  w9Obj = new getW9Data();
  bankdet = new Array();
  bankObj = new getBankDetails();
  @ViewChild('fileUploader') fileUploader: ElementRef<HTMLInputElement>;
  fileContentBuffer: any;
  public maskTIN = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private datePipe: DatePipe, private router: Router, public ddlist: DdlistService, private toastr: ToastrService, private od: OwnerDetailsService) {
    this.datepickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      minMode: this.minMode,
      dateInputFormat: 'YYYY',
      adaptivePosition: true
    });
  }

  ngOnInit() {
    this.url = this.router.url;
    this.url = this.url.split('/')
    console.log(this.url)
    if (this.url[3] == 'uploadw9') {
      this.getDDlistName('Bnk_acc_Type').subscribe(response => { });
      this.getDDlistName('Company_Type').subscribe(response => { });
      this.getCountryDDList('CNT').subscribe(response => { });
      this.getw9Data();
      this.selectBankDetails();
      this.isSubmitted = true;
    } else {
      this.isSubmitted = true;
      this.isShowCheckBox = false;
    }
  }

  openW9From() {
    this.showW9 = true;
  }

  openBankForm() {
    this.showBankModal = true;
  }

  w9Form = new FormGroup({
    p_w9_id: new FormControl(-1),
    p_comp_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).comp_id),
    p_owner_id: new FormControl(''),
    p_w9_county: new FormControl('', Validators.required),
    p_w9_country: new FormControl('', Validators.required),
    p_w9_state: new FormControl('', Validators.required),
    p_w9_city: new FormControl('', Validators.required),
    p_w9_tin_number: new FormControl(''),
    p_w9_tin_type: new FormControl(''),
    p_w9_address: new FormControl(''),
    p_w9_zip: new FormControl('', Validators.required),
    p_w9_year_from: new FormControl(''),
    p_w9_year_to: new FormControl(''),
    p_doc_img: new FormControl(''),
    p_file_name: new FormControl(''),
    p_active: new FormControl(true),
    p_comments: new FormControl(''),
    p_usr_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).usr_id)
  });


  bankForm = new FormGroup({
    p_acct_id: new FormControl(-1),
    p_comp_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).comp_id),
    p_acct_holder_id: new FormControl(''),
    p_acct_type: new FormControl(''),
    p_account_number: new FormControl(''),
    p_routing_number: new FormControl(''),
    p_usr_country: new FormControl(''),
    p_usr_state: new FormControl(''),
    p_usr_county: new FormControl(''),
    p_city_name: new FormControl(''),
    p_usr_zip: new FormControl(''),
    p_address: new FormControl(''),
    p_comments: new FormControl(''),
    p_active: new FormControl(true),
    p_usr_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).usr_id)
  })

  getDDlistName(listname: string) {
    return this.ddlist.getDDlist(listname)
      .pipe(
        /* use tap() to set `this.ddlistname` */
        tap(response => {
          if (response.success) {
            if (listname == 'Bnk_acc_Type') {
              this.accDDType = response.data
            } else {
              this.ddlistname = response.data;
            }
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  getCountryDDList(code: string) {
    return this.ddlist.countryDropDown(code).pipe(
      tap(response => {
        if (response.success) {
          this.country = response.data;
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  OnCountrySelect(val) {
    if (val != null) {
      this.getStateDDList('ST', this.country.find(x => x.country_id == val.target.value).country_code).subscribe(response => { console.log(response) });
    }
  }

  getStateDDList(code: string, search: string) {
    return this.ddlist.stateDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.state = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnStateSelect(val) {
    if (val != null) {
      this.getCountyDDList('CY', this.state.find(x => x.state_id == val.target.value).state_code).subscribe(response => { console.log(response) });
    }
  }

  getCountyDDList(code: string, search: string) {
    return this.ddlist.countyDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.county = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnCountySelect(val) {
    if (val != null) {
      this.getCityDDList('CT', this.county.find(x => x.county_id == val.target.value).county_name).subscribe(response => { console.log(response) });
    }
  }

  getCityDDList(code: string, search: string) {
    return this.ddlist.cityDropDown(code, search)
      .pipe(
        tap(response => {
          if (response.success) {
            this.city = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  OnCitySelect(name) {
    if (name.target.value != null) {
      this.getZipcode(name.target.value).subscribe(response => { })
    }
  }

  getZipcode(name) {
    return this.ddlist.zipCode(name)
      .pipe(
        tap(response => {
          if (response.success) {
            this.zipCode = response.data;
            this.zipCode = this.zipCode.map(x => x.zipcode)
            this.w9Form.patchValue({ p_w9_zip: this.zipCode[0] });
            this.bankForm.patchValue({ p_usr_zip: this.zipCode[0] })
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  fileToBase64 = file => {
    return new Promise(resolve => {
      var reader = new FileReader();
      // Read file content on file loaded event
      reader.onload = function (event) {
        resolve(reader.result);
      };
      // Convert data to base64
      reader.readAsDataURL(file);
    });
  };

  getBase64(event) {
    let file = event.target.files[0];
    let name = file.name
    this.fileToBase64(file).then(results => {
      this.fileContentBuffer = results;
      console.log(this.fileContentBuffer);
      this.w9Form.patchValue({
        p_doc_img: this.fileContentBuffer,
        p_file_name: name
      });
    });
  }

  resetFileUploader() {
    this.fileUploader.nativeElement.value = null;
  }

  uploadw9() {
    if (this.w9Form.valid) {
      const formValue = this.w9Form.value;
      formValue.p_owner_id = parseInt(this.url[4])
      formValue.p_w9_tin_number = new RegExp(this.w9Form.get('p_w9_tin_number').value).source.replace(/-/g, "");
      formValue.p_w9_zip = new RegExp(this.w9Form.get('p_w9_zip').value).source.replace(/-/g, "");
      formValue.p_w9_year_from = this.datePipe.transform(this.w9Form.get('p_w9_year_from').value, 'yyyy');
      formValue.p_w9_year_to = this.datePipe.transform(this.w9Form.get('p_w9_year_to').value, 'yyyy');
      this.od.uploadw9Form(JSON.stringify(formValue)).subscribe(response => {
        if (response.success) {
          this.toastr.success('Saved Successfully', 'W9Data');
          this.w9Form.reset();
          this.resetFileUploader();
        } else {
          this.toastr.error(response.errorMessage);
        }
      });
    } else {
      this.toastr.error('Form Cannot Be empty');
    }
  }

  getw9Data() {
    this.w9Obj.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.w9Obj.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.w9Obj.p_owner_id = this.url[4];
    this.w9Obj.p_w9_id = -1;
    this.od.getw9Data(this.w9Obj).subscribe(response => {
      if (response.success) {
        this.w9details = response.data;
      } else {
        this.toastr.error(response.errorMessage, 'W9Data');
      }
    });
  }

  editw9Data(element) {
    console.log(element, this.url[4])
  }

  addBankDetails() {
    if (this.bankForm.valid) {
      const bForm = this.bankForm.value
      bForm.p_acct_holder_id = parseInt(this.url[4]);
      bForm.p_usr_zip = new RegExp(this.bankForm.get('p_usr_zip').value).source.replace(/-/g, "");
      this.od.addbankDetails(JSON.stringify(this.bankForm.value)).subscribe(response => {
        if (response.success) {
          this.toastr.success('Saved Successfully', 'Bankdata');
          this.bankForm.reset();
        } else {
          this.toastr.error(response.errorMessage, 'Bankdata');
        }
      })
    } else {
      this.toastr.error('Form Cannot be Invalid');
    }
  }

  selectBankDetails() {
    this.bankObj.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.bankObj.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.bankObj.p_acct_id = -1;
    this.bankObj.p_acct_holder_id = -1;
    this.bankObj.p_type = 'PO';
    this.od.getBankDetails(this.bankObj).subscribe(response => {
      if (response.success) {
        this.bankdet = response.data;
      } else {
        this.toastr.error(response.errorMessage, 'BankData');
      }
    });
  }

}
