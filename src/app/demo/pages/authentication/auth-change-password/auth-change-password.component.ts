import { Component, OnInit } from '@angular/core';
import { AuthSigninService } from 'src/app/Services/auth-signin.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ChangePassword } from 'src/app/common_models/user_model';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-auth-change-password',
  templateUrl: './auth-change-password.component.html',
  styleUrls: ['./auth-change-password.component.scss']
})
export class AuthChangePasswordComponent implements OnInit {

  cp = new ChangePassword();
  obj = localStorage.getItem('currentuser')
  reEnterPswd: string;

  constructor(private auth: AuthSigninService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
  }

  changePassword() {
    this.cp.p_login_usr_id = JSON.parse(this.obj).usr_id;
    this.cp.p_reset_by_usr_id = JSON.parse(this.obj).usr_id;
    if (isNullOrUndefined(this.cp.p_usr_pswd_old) && isNullOrUndefined(this.cp.p_usr_pswd_new)) {
      this.toastr.warning('Please fill all fields');
    } else if (isNullOrUndefined(this.cp.p_usr_pswd_old)) {
      this.toastr.warning('Please Enter Current Password');
    } else if (isNullOrUndefined(this.cp.p_usr_pswd_new)) {
      this.toastr.warning('New Password cannot be blank or undefined');
    } else if (isNullOrUndefined(this.cp.p_usr_pswd_new)) {
      this.toastr.warning('New Password cannot be blank or undefined');
    } else {
      if (this.cp.p_usr_pswd_new == this.reEnterPswd) {
        this.auth.resetPassword(this.cp).subscribe(response => {
          if (response.success) {
            this.toastr.success('Password Changed Successfully');
            this.router.navigate(['/auth/signin']);
          } else {
            this.toastr.error(response.errorMessage);
          }
        });
      } else {
        this.toastr.error('New Password Didnt Match, Try Again');
      }
    }
  }

}
