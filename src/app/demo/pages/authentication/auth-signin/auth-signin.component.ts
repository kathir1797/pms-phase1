import { Component, OnInit, HostListener } from '@angular/core';
import { login, User } from 'src/app/common_models/user_model';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import { AuthSigninService } from 'src/app/Services/auth-signin.service';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37,
  Enter = "Enter"
}

@Component({
  selector: 'app-auth-signin',
  templateUrl: './auth-signin.component.html',
  styleUrls: ['./auth-signin.component.scss']
})
export class AuthSigninComponent implements OnInit {

  userData: login = new login();

  constructor(private router: Router, private authsignin: AuthSigninService, private toastr: ToastrService) { }

  ngOnInit() {
  }
  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.code == KEY_CODE.Enter) {
      this.onLoggedIn();
    }
  }

  onLoggedIn(): Observable<User> {
    if (isNullOrUndefined(this.userData.pusr_login) && isNullOrUndefined(this.userData.pusr_paswd)) {
      console.log('user logg')
      this.toastr.error('Enter Email ID and Password', 'Login');
      // this.router.navigate(['/error']);
      return
    } else if (isNullOrUndefined(this.userData.pusr_login) || this.userData.pusr_login.trim() === '') {
      this.toastr.error('Please enter valid Email ID', 'Login');
    } else if (isNullOrUndefined(this.userData.pusr_paswd) || this.userData.pusr_paswd.trim() === '') {
      this.toastr.error('Please enter password.', 'Login');
    }
    else {
      this.authsignin.login(this.userData).subscribe((response) => {
        if (response.success) {
          localStorage.setItem('currentuser', JSON.stringify(response.data));
          // this.toastr.success('Login Successful', 'Login')
          // this.router.navigate(['/default/dashboard']);
          if (response.data.pswd_reset == true) {
            this.router.navigate(['/auth/change-password']);
          } else {
            this.toastr.success('Login Successful', 'Login');
            this.router.navigate(['/default/dashboard']);
          }

        }
        else {
          this.toastr.error(response.errorMessage, 'Login');
        }
      });
    }
  }

  // onLoggedIn() : Observable<User>{
  //   if(isNullOrUndefined(this.userData.username) ||  isNullOrUndefined(this.userData.pswd) 
  //   || this.userData.username === '' || this.userData.pswd === ''){
  //     this.router.navigate(['/default/error']);
  //   }
  //   else{
  //     this.authsignin.login(this.userData).map()
  //   }


  //   //console.log('Hello');
  //   // if(this.userData.username === 'admin@alphind.in' && this.userData.pswd==='letmein')
  //   // {
  //   //   this.router.navigate(['/default/dashboard']);
  //   // }
  // }

}
