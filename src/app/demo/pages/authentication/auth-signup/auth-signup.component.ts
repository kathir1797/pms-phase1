import { Component, OnInit } from '@angular/core';
import { AuthSigninService } from 'src/app/Services/auth-signin.service';
import { ContactUs } from 'src/app/common_models/user_model';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth-signup',
  templateUrl: './auth-signup.component.html',
  styleUrls: ['./auth-signup.component.scss']
})
export class AuthSignupComponent implements OnInit {

  // contact: ContactUs;

  constructor(private as : AuthSigninService, private router: Router, private toastr : ToastrService) { }

  ngOnInit() {
  }

  form = new FormGroup({
    p_form_id : new FormControl(-1),
    p_comp_name : new FormControl('', Validators.required),
    p_fname: new FormControl('', Validators.required),
    p_lname: new FormControl('', Validators.required),
    p_mail : new FormControl('', [Validators.required, Validators.email]),
    p_phone: new FormControl('', Validators.required)
  });

  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  contactUs(){
    // this.contact.p_form_id = -1;
    if(this.form.valid) {
      this.as.contactus(this.form.value).subscribe(response => {
        if(response.success) {
          this.toastr.success('Registered Successfully', 'ContactUs');
          this.form.reset();
        } else {
          this.toastr.error(response.errorMessage);
        }
      });
    } else {
      this.validateAllFormFields(this.form);
      this.toastr.error('Required Field information is not available. Please enter required information in highlighted fields','ContactUs');
    }
  }

}
