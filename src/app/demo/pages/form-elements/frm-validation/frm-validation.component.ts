import { Component, OnInit } from '@angular/core';
import { Company, ddlist, StateDDList, CountyDDList } from 'src/app/common_models/company_model';
import { Router } from '@angular/router';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { GetCompaniesService } from 'src/app/Services/getCompanies.service';
import { NgForm } from '@angular/forms';

export class FormInput {
  email: any;
  password: any;
  confirmPassword: any;
  requiredInput: any;
  url: any;
  phone: any;
  cmbGear: any;
  address: any;
  file: any;
  switcher: any;
}

@Component({
  selector: 'app-frm-validation',
  templateUrl: './frm-validation.component.html',
  styleUrls: ['./frm-validation.component.scss']
})
export class FrmValidationComponent implements OnInit {
  formInput: FormInput;
  
  public isSubmit: boolean;
  constructor() {
    this.isSubmit = false;
  }

  ngOnInit() {
  }

}
