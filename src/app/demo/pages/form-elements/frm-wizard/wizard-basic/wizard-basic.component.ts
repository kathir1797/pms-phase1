import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PropertyService } from 'src/app/Services/property.service';

@Component({
  selector: 'app-wizard-basic',
  templateUrl: './wizard-basic.component.html',
  styleUrls: ['./wizard-basic.component.scss']
})
export class WizardBasicComponent implements OnInit {

  constructor(private router: Router, public ddlist: DdlistService, public cs: PropertyService, private toastr: ToastrService) { }

  ngOnInit() {
  }

  form = new FormGroup({
    p_prop_id : new FormControl(-1),
    p_comp_id : new FormControl(1),
    p_owner_id : new FormControl(-1),
    p_prop_name : new FormControl('', Validators.required),
    p_prop_type : new FormControl('',Validators.required),
    p_prop_mode: new FormControl('',Validators.required),
    p_prop_zone: new FormControl('',Validators.required),
    p_bhk : new FormControl('',Validators.required),
    p_floor: new FormControl('',Validators.required),
    p_total_floor: new FormControl('',Validators.required),
    p_prop_status: new FormControl('',Validators.required),
    p_prop_size: new FormControl('',Validators.required),
    p_room_count: new FormControl('',Validators.required),
    p_amenities: new FormControl('',Validators.required),
    p_elect_prov: new FormControl('',Validators.required),
    p_elect_ph: new FormControl('',Validators.required),
    p_gas_prov: new FormControl('',Validators.required),
    p_gas_ph: new FormControl('',Validators.required),
    p_water_prov: new FormControl('',Validators.required),
    p_water_ph: new FormControl('',Validators.required),
    p_city: new FormControl('',Validators.required),
    p_county: new FormControl('',Validators.required),
    p_state: new FormControl('',Validators.required),
    p_parking_space: new FormControl('',Validators.required),
    p_address: new FormControl('',Validators.required),
    p_address1: new FormControl('',Validators.required),
    p_zip: new FormControl('',Validators.required),
    p_eff_date: new FormControl('',Validators.required),
    p_end_date: new FormControl('',Validators.required),
    p_active: new FormControl(true),
    p_usr_id: new FormControl(1)
  })

  get f() {
    return this.form.controls;
  }

  addProperty(){
    this.cs.addProperty(JSON.stringify(this.form.value)).subscribe(response => {
      if(response.success){
        this.toastr.success('Property Added successfully', 'Property', {positionClass : 'toast-bottom-right'});
        this.form.reset();
      } else{
        this.toastr.error(response.errorMessage, 'Property', {positionClass : 'toast-bottom-right'});
      }
    })
  }
}
