import { Component, OnInit } from "@angular/core";
import Stepper from "bs-stepper";
import {
  FormGroup,
  FormControl,
  Validators,
  FormControlName,
} from "@angular/forms";
import { Router } from "@angular/router";
import { DdlistService } from "src/app/Services/ddlist.service";
import { PropertyService } from "src/app/Services/property.service";
import { ToastrService } from "ngx-toastr";
import { GetProperty } from "src/app/common_models/property_model";
import { tap, map, switchMap } from "rxjs/operators";
import { forkJoin, of } from "rxjs";
import Swal from "sweetalert2";

@Component({
  selector: "app-add-property",
  templateUrl: "./add-property.component.html",
  styleUrls: ["./add-property.component.css"],
})
export class AddPropertyComponent implements OnInit {
  private stepper: Stepper;
  public getprop = new GetProperty();
  url: any;
  isSubmitted: boolean;
  isShowCheckBox: boolean = false;
  public maskTeleUS = [
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];
  public maskZipCode = [
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];
  state = new Array();
  city = new Array();
  county = new Array();
  country = new Array();
  pz = new Array();
  pt = new Array();
  bhk = new Array();
  pa = new Array();
  ps = new Array();
  pm = new Array();
  ft = new Array();
  zipCode = new Array();

  constructor(
    private router: Router,
    public ddlist: DdlistService,
    public cs: PropertyService,
    private toastr: ToastrService
  ) {
    this.url = this.router.url;
    this.url = this.url.split("/");
  }

  ngOnInit() {
    this.stepper = new Stepper(document.querySelector("#stepper1"), {
      linear: false,
      animation: true,
    });
    this.getCountryDDList("CNT").subscribe((response) => {
      console.log(response);
    });
    this.getDDlistName("Prop_Zoning").subscribe((response) => {
      console.log(response);
    });
    this.getDDlistName("Prop_Type").subscribe((response) => {
      console.log(response);
    });
    this.getDDlistName("BHK_Type").subscribe((response) => {
      console.log(response);
    });
    this.getDDlistName("Prop_Age").subscribe((response) => {
      console.log(response);
    });
    this.getDDlistName("Prop_Status").subscribe((response) => {
      console.log(response);
    });
    this.getDDlistName("Floor_Type").subscribe((response) => {
      console.log(response);
    });
    this.getDDlistName("Prop_Mode").subscribe((response) => {
      console.log(response);
    });
    if (this.url[3] == "editproperty") {
      this.getProperty();
      this.isShowCheckBox = true;
      this.isSubmitted = false;
      console.log(this.url);
      this.form.get("p_active").value;
    } else {
      this.isSubmitted = true;
      this.isShowCheckBox = false;
    }
  }

  form = new FormGroup(
    {
      p_prop_id: new FormControl(-1),
      p_comp_id: new FormControl(
        JSON.parse(localStorage.getItem("currentuser")).comp_id
      ),
      p_owner_id: new FormControl(null),
      p_prop_name: new FormControl(""),
      p_prop_type: new FormControl("", Validators.required),
      p_prop_mode: new FormControl("", Validators.required),
      p_prop_zone: new FormControl("", Validators.required),
      p_bhk: new FormControl(-1),
      p_floor: new FormControl("", Validators.required),
      p_total_floor: new FormControl("", Validators.required),
      p_prop_status: new FormControl("", Validators.required),
      p_prop_size: new FormControl("", Validators.required),
      p_room_count: new FormControl("", Validators.required),
      p_amenities: new FormControl(""),
      p_elect_prov: new FormControl("", Validators.required),
      p_elect_ph: new FormControl("", Validators.required),
      p_gas_prov: new FormControl("", Validators.required),
      p_gas_ph: new FormControl("", Validators.required),
      p_water_prov: new FormControl("", Validators.required),
      p_water_ph: new FormControl("", Validators.required),
      p_city: new FormControl("", Validators.required),
      p_county: new FormControl("", Validators.required),
      p_state: new FormControl("", Validators.required),
      p_country: new FormControl("", Validators.required),
      p_parking_space: new FormControl("", Validators.required),
      p_address: new FormControl("", Validators.required),
      p_no_of_bath: new FormControl("", Validators.required),
      p_address1: new FormControl(null),
      p_zip: new FormControl("", Validators.required),
      p_comments: new FormControl(""),
      p_prop_age: new FormControl("", Validators.required),
      p_active: new FormControl(true),
      p_usr_id: new FormControl(
        JSON.parse(localStorage.getItem("currentuser")).usr_id
      ),
    },
    {
      validators: [
        this.cs.rangeValidator("p_total_floor"),
        this.cs.propSizeValidator("p_prop_size"),
        this.cs.rangeValidator("p_room_count"),
        this.cs.rangeValidator("p_no_of_bath"),
        this.cs.parkingSpaceValidator("p_parking_space"),
      ],
    }
  );

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  next() {
    this.stepper.next();
  }

  previous() {
    this.stepper.previous();
  }

  // onSubmit() {
  //   return this.form.status;
  // }

  get f() {
    return this.form.controls;
  }

  getDDlistName(listname: string) {
    return this.ddlist.getDDlist(listname).pipe(
      /* use tap() to set `this.ddlistname` */
      tap((response) => {
        if (response.success) {
          if (listname == "Prop_Zoning") {
            this.pz = response.data;
          } else if (listname == "Prop_Type") {
            this.pt = response.data;
          } else if (listname == "Prop_Age") {
            this.pa = response.data;
          } else if (listname == "Prop_Status") {
            this.ps = response.data;
          } else if (listname == "Floor_Type") {
            this.ft = response.data;
          } else if (listname == "Prop_Mode") {
            this.pm = response.data;
          }
        } else {
          this.toastr.info(response.errorMessage, "Property");
        }
      })
    );
  }

  getCountryDDList(code: string) {
    return this.ddlist.countryDropDown(code).pipe(
      tap((response) => {
        if (response.success) {
          this.country = response.data;
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  OnCountrySelect(val) {
    if (val != null) {
      this.getStateDDList(
        "ST",
        this.country.find((x) => x.country_id == val.target.value).country_code
      ).subscribe((response) => {
        console.log(response);
      });
    }
  }

  getStateDDList(code: string, search: string) {
    return this.ddlist.stateDropDown(code, search).pipe(
      tap((response) => {
        if (response.success) {
          this.state = response.data;
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  OnStateSelect(val) {
    if (val != null) {
      this.getCountyDDList(
        "CY",
        this.state.find((x) => x.state_id == val.target.value).state_code
      ).subscribe((response) => {
        console.log(response);
      });
    }
  }

  getCountyDDList(code: string, search: string) {
    return this.ddlist.countyDropDown(code, search).pipe(
      tap((response) => {
        if (response.success) {
          this.county = response.data;
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  OnCountySelect(val) {
    if (val != null) {
      this.getCityDDList(
        "CT",
        this.county.find((x) => x.county_id == val.target.value).county_name
      ).subscribe((response) => {
        console.log(response);
      });
    }
  }

  getCityDDList(code: string, search: string) {
    return this.ddlist.cityDropDown(code, search).pipe(
      tap((response) => {
        if (response.success) {
          this.city = response.data;
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  OnCitySelect(name) {
    if (name != null) {
      this.getZipcode(name.target.value).subscribe((response) => { });
    }
  }

  getZipcode(name) {
    return this.ddlist.zipCode(name).pipe(
      tap((response) => {
        if (response.success) {
          this.zipCode = response.data;
          this.zipCode = this.zipCode.map((x) => x.zipcode);
          this.form.patchValue({ p_zip: this.zipCode[0] });
        } else {
          this.toastr.error(response.errorMessage);
        }
      })
    );
  }

  addProperty() {
    if (this.form.valid) {
      const formValue = this.form.value
      formValue.p_elect_ph = new RegExp(this.form.get('p_elect_ph').value).source.replace(/-/g, "");
      formValue.p_gas_ph = new RegExp(this.form.get('p_gas_ph').value).source.replace(/-/g, "");
      formValue.p_water_ph = new RegExp(this.form.get('p_water_ph').value).source.replace(/-/g, "");
      formValue.p_zip = new RegExp(this.form.get('p_zip').value).source.replace(/-/g, "");
      this.cs
        .addProperty(JSON.stringify(formValue))
        .subscribe((response) => {
          if (response.success) {
            if (this.form.controls["p_active"].value == false) {
              Swal.fire("Property", "Deactivated Successfully");
            }
            this.toastr.success("Saved successfully", "Property");
            this.router.navigate(["/property/add"]);
            this.form.reset();
          } else {
            this.toastr.error(response.errorMessage, "Property");
          }
        });
    } else {
      this.validateAllFormFields(this.form);
      this.toastr.error(
        "Required Field information is not available. Please enter required information in highlighted fields"
      );
    }
  }

  getProperty() {
    this.getprop.p_comp_id = JSON.parse(
      localStorage.getItem("currentuser")
    ).comp_id;
    this.getprop.p_usr_id = JSON.parse(
      localStorage.getItem("currentuser")
    ).usr_id;
    this.getprop.p_prop_id = this.url[4];
    forkJoin([
      this.getCountryDDList("CNT"),
      this.getStateDDList('ST', this.url[5]),
      this.getCountyDDList('CY', this.url[6]),
      this.getDDlistName("Prop_Zoning"),
      this.getDDlistName("Prop_Type"),
      this.getDDlistName("Prop_Age"),
      this.getDDlistName("Prop_Status"),
      this.getDDlistName("Floor_Type"),
      this.getDDlistName("Prop_Mode"),
      this.getCityDDList("CT", this.url[7]),
    ])
      .pipe(
        switchMap((results) => {
          if (
            results[0].success &&
            results[1].success &&
            results[2].success &&
            results[3].success &&
            results[4].success &&
            results[5].success &&
            results[6].success &&
            results[7].success &&
            results[8].success &&
            results[9].success
          ) {
            this.country = results[0].data;
            this.state = results[1].data;
            this.county = results[2].data;
            this.pz = results[3].data;
            this.pt = results[4].data;
            this.pa = results[5].data;
            this.ps = results[6].data;
            this.ft = results[7].data;
            this.pm = results[8].data;
            this.city = results[9].data;
          } else {
            this.toastr.warning("No data found in Dropdown");
          }
          return this.cs.getProperty(this.getprop);
        })
      )
      .subscribe((response) => {
        if (response.success) {
          let data = response.data;
          let age = this.pa.filter((value) => {
            if (value.dd_value == data[0].prop_age_desc) {
              return value;
            }
          });

          let prop_type = this.pt.filter((value) => {
            if (value.dd_value == data[0].ppt_type) {
              return value;
            }
          });

          let prop_zone = this.pz.filter((value) => {
            if (value.dd_value == data[0].ppt_zone) {
              return value;
            }
          });

          let floor_type = this.ft.filter((value) => {
            if (value.dd_value == data[0].ppt_floor) {
              return value;
            }
          });

          let prop_status = this.ps.filter((value) => {
            if (value.dd_value == data[0].ppt_status) {
              return value;
            }
          });

          let propmode = this.pm.filter((value) => {
            if (value.dd_value == data[0].ppt_mode) {
              return value;
            }
          });

          let county = this.county.filter((value) => {
            if (value.county_name == data[0].county_name) {
              return value;
            }
          });

          let state = this.state.filter((value) => {
            if (value.state_name == data[0].state_name) {
              return value;
            }
          });

          let country = this.country.filter((value) => {
            if (value.country_name == data[0].country_name) {
              return value;
            }
          });
          let city = this.city.filter(value => { return value ? value.city_name == data[0].city_name : this.toastr.error('city not matched') })
          this.form.get("p_prop_id").setValue(data[0].prop_id);
          this.form.get("p_prop_name").setValue(data[0].prop_name);
          this.form.get("p_total_floor").setValue(data[0].total_floor);
          this.form.get("p_prop_size").setValue(data[0].prop_size);
          this.form.get("p_amenities").setValue(data[0].other_amenities);
          this.form.get("p_elect_prov").setValue(data[0].electricity_prov);
          this.form.get("p_elect_ph").setValue(data[0].electricity_prov_ph);
          this.form.get("p_gas_prov").setValue(data[0].gas_prov);
          this.form.get("p_gas_ph").setValue(data[0].gas_prov_ph_no);
          this.form.get("p_comments").setValue(data[0].prop_neigh);
          this.form.get("p_zip").setValue(data[0].prop_zip_code);
          this.form.get("p_water_prov").setValue(data[0].water_prv);
          this.form.get("p_water_ph").setValue(data[0].water_prv_phone_no);
          this.form.get("p_room_count").setValue(data[0].no_of_rooms);
          this.form.get("p_active").setValue(data[0].active);
          // this.form.get('p_address').setValue(data[0].prop_address1)no_of_bathroom
          this.form.get("p_address").setValue(data[0].prop_address1);
          this.form.get("p_no_of_bath").setValue(data[0].no_of_bathroom);
          this.form.get("p_parking_space").setValue(data[0].parking_space);
          // this.form.controls['p_prop_age'].setValue(age[0].dd_det_id, { onlySelf: true });
          // this.form.controls['p_prop_zone'].setValue(prop_zone[0].dd_det_id, { onlySelf: true });
          // this.form.controls['p_state'].setValue(state[0].state_id, { onlySelf: true });
          // this.form.controls['p_prop_status'].setValue(prop_status[0].dd_det_id, { onlySelf: true });
          // this.form.controls['p_floor'].setValue(floor_type[0].dd_det_id, { onlySelf: true });
          // // this.form.controls['p_bhk'].setValue(bhk[0].dd_det_id, { onlySelf: true });
          // this.form.controls['p_prop_mode'].setValue(propmode[0].dd_det_id, { onlySelf: true });
          // this.form.controls['p_city'].setValue(city[0].city_id, { onlySelf: true });
          // this.form.controls['p_county'].setValue(county[0].county_id, { onlySelf: true });
          // this.form.controls['p_country'].setValue(country[0].country_id, { onlySelf: true });
          this.form.controls["p_prop_type"].setValue(prop_type[0].dd_det_id, {
            onlySelf: true,
          });
          this.form.patchValue({
            p_prop_age: age[0].dd_det_id,
            p_prop_zone: prop_zone[0].dd_det_id,
            p_prop_status: prop_status[0].dd_det_id,
            p_prop_mode: propmode[0].dd_det_id,
            p_floor: floor_type[0].dd_det_id,
            p_country: country[0].country_id,
            p_state: state[0].state_id,
            p_county: county[0].county_id,
            p_city: city[0].city_name,
          });
        } else {
          this.toastr.error(response.errorMessage, "Property");
        }
      });
  }
}
