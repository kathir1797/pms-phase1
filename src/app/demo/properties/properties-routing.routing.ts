import { Routes, RouterModule } from '@angular/router';
import { PropertiesComponent } from './properties.component';
import { AddPropertyComponent } from './add-property/add-property.component';

const routes: Routes = [
  {
    path: '',
    component: PropertiesComponent
  },
  {
    path: 'addproperty',
    component: AddPropertyComponent
  },
  {
    path : 'editproperty/:id/:country/:state/:county',
    component: AddPropertyComponent
  }
];

export const PropertiesRoutingRoutes = RouterModule.forChild(routes);
