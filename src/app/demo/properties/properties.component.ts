import { Component, OnInit } from '@angular/core';
import Stepper from 'bs-stepper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { PropertyService } from 'src/app/Services/property.service';
import { ToastrService } from 'ngx-toastr';
import { GetProperty } from 'src/app/common_models/property_model';
@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.css']
})
export class PropertiesComponent implements OnInit {


  private stepper: Stepper;
  url: any;
  public getprop = new GetProperty();
  propdet = new Array;
  searchText: any;
  p: number = 1;
  totalRecords: number;
  constructor(private router: Router, public ddlist: DdlistService, public cs: PropertyService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getProperty();
  }

  editProperty(property) {
    console.log(property);
    this.router.navigate(['/property/add/editproperty', property.prop_id, property.country_code, property.state_code, property.county_name]);
  }

  addIncomeForProperty(propertyId) {
    this.router.navigate(['/finance/income/propinc', propertyId]);
  }

  addExpenseForProperty(propertyId) {
    this.router.navigate(['/finance/expenditure/propexp', propertyId]);
  }

  getProperty() {
    this.getprop.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getprop.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.getprop.p_prop_id = null;
    this.cs.getProperty(this.getprop).subscribe(response => {
      if (response.success) {
        console.log(response.data);
        this.propdet = response.data;
        this.totalRecords = response.data.length;
      } else {
        this.toastr.info(response.errorMessage, 'Property')
      }
    })
  }

  yesnoCheck() {
    const ele = document.getElementById('yesCheck') as HTMLInputElement;
    if (ele.checked) {
      document.getElementById('owner').style.display = 'block';
      document.getElementById('ownersearch').style.display = 'block';
      document.getElementById('address').style.display = 'none';
      document.getElementById('address1').style.display = 'none';
      document.getElementById('adressSearch').style.display = 'none';
      // document.getElementById('owner').style.visibility = 'visible';
      // document.getElementById('address').style.visibility = 'hidden';
      // document.getElementById('address1').style.visibility = 'hidden';

      //<a [routerLink]="['/finance/income/']">Add Income</a><br>
      // <a [routerLink]="['/finance/expenditure/']">Add Expenditure</a>
    }
    else {
      document.getElementById('address').style.display = 'block';
      document.getElementById('address1').style.display = 'block';
      document.getElementById('adressSearch').style.display = 'block';
      document.getElementById('owner').style.display = 'none';
      document.getElementById('ownersearch').style.display = 'none';
    }

  }

  deleteFieldValue(index) {
    this.propdet.splice(index, 1);
    this.toastr.info('Deleted Successfully', 'Property')
  }
}
