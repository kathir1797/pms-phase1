import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertiesComponent } from './properties.component';
import { PropertiesRoutingRoutes } from './properties-routing.routing';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { AddPropertyComponent } from './add-property/add-property.component';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { TextMaskModule } from 'angular2-text-mask';
/*import {CurrencyMaskModule} from 'ng2-currency-mask';*/
import { NgNumberFormatterModule } from 'ng-number-formatter';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';

@NgModule({
  imports: [
    CommonModule,
    PropertiesRoutingRoutes,
    SharedModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    TextMaskModule,
    NgNumberFormatterModule,
    TypeaheadModule.forRoot()

  ],
  declarations: [PropertiesComponent, AddPropertyComponent]
})
export class PropertiesModule { }
