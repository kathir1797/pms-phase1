import { Component, OnInit } from "@angular/core";
import { ReportsService } from "src/app/Services/reports.service";
import { GetPropertyReports, GetOwnerReports, GetTenantReports, MonthlyReport, GetLeaseReports } from "src/app/common_models/reports";
import { ToastrService } from "ngx-toastr";
import { ExportToCsv } from "export-to-csv";
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/';
import { tap } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { ExportAsConfig, ExportAsService } from 'ngx-export-as';

import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: "app-reports",
  templateUrl: "./reports.component.html",
  styleUrls: ["./reports.component.css"],
})
export class ReportsComponent implements OnInit {
  public propreports = new GetPropertyReports();
  public ownerReports = new GetOwnerReports();
  public tenantReports = new GetTenantReports();
  public monthlyRent = new MonthlyReport();
  public getlease = new GetLeaseReports();
  result1 = new Array();
  result2 = new Array();
  result3 = new Array();
  result4 = new Array();
  result5 = new Array();
  rentDetails = new Array();
  searchProperty: string;
  searchOwner: string;
  searchTenant: string;
  searchLease: string;
  totalProperties: number;
  totalOwners: number;
  totalTenant: number;
  totalRent: number
  totalLease: number;
  p: number = 1;
  fromDate = new Date();
  toDate = new Date();
  datepickerConfig: Partial<BsDatepickerConfig>;
  columns = [];
  showModal: boolean;
  showOwners: boolean;
  showTenant: boolean;
  showLease: boolean;
  showRent: boolean;
  show1099: boolean;

  constructor(private datePipe: DatePipe, private reports: ReportsService, private toastr: ToastrService) {
    this.datepickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      dateInputFormat: 'MM-DD-YYYY',
      isAnimated: true,
      showWeekNumbers: false
    });
  }

  ngOnInit() {
  }

  getActiveProperty() {
    this.showModal = true;
    this.propreports.p_usr_id = JSON.parse(localStorage.getItem("currentuser")).usr_id;
    this.propreports.p_comp_id = JSON.parse(localStorage.getItem("currentuser")).comp_id;
    this.reports.getPropReport(this.propreports).subscribe(response => {
      if (response.success) {
        this.result1 = response.data;
        this.totalProperties = response.data.length;
      } else {
        this.toastr.error(response.errorMessage, 'Reports');
      }
    });
  }

  getActiveOwner() {
    this.showOwners = true;
    this.ownerReports.p_usr_id = JSON.parse(localStorage.getItem("currentuser")).usr_id;
    this.ownerReports.p_comp_id = JSON.parse(localStorage.getItem("currentuser")).comp_id;
    this.reports.getOwnerReport(this.ownerReports).subscribe(response => {
      if (response.success) {
        this.result2 = response.data;
        this.totalOwners = response.data.length;
      } else {
        this.toastr.error(response.errorMessage, 'Reports');
      }
    });
  }

  getActiveTenant() {
    this.showTenant = true;
    this.tenantReports.p_usr_id = JSON.parse(localStorage.getItem("currentuser")).usr_id;
    this.tenantReports.p_comp_id = JSON.parse(localStorage.getItem("currentuser")).comp_id;
    this.reports.getTenantReport(this.tenantReports).subscribe(response => {
      if (response.success) {
        this.result3 = response.data;
        this.totalTenant = response.data.length;
      } else {
        this.toastr.error(response.errorMessage, 'Reports');
      }
    });
  }

  getLease() {
    this.showLease = true;
    this.getlease.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getlease.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.reports.getLeaseReport(this.getlease).subscribe(response => {
      if (response.success) {
        this.result5 = response.data;
        this.totalLease = response.data.length;
      } else {
        this.toastr.error(response.errorMessage, 'Download Lease');
      }
    });
  }

  openRentReport() {
    this.showRent = true;
  }

  generateMonthlyRentReport() {
    this.monthlyRent.p_comp_id = JSON.parse(localStorage.getItem("currentuser")).comp_id;
    this.monthlyRent.p_from_date = this.datePipe.transform(this.fromDate, 'MM-dd-yyyy');
    this.monthlyRent.p_to_date = this.datePipe.transform(this.toDate, 'MM-dd-yyyy');
    this.monthlyRent.p_usr_id = JSON.parse(localStorage.getItem("currentuser")).usr_id;
    this.reports.generateMonthlyRentReport(this.monthlyRent).subscribe(response => {
      if (response.success) {
        this.columns = ["Owner Name", "Property Address", "Income Amount", "Expense Amount", "Net Amount"];
        this.rentDetails = response.data;
        console.log(this.rentDetails, typeof this.rentDetails);
      } else {
        this.toastr.error(response.errorMessage, 'Reports');
      }
    });
  }

  downloadMonthlyRentReport() {
    this.generateMonthlyRentReport();
    const options = {
      fieldSeparator: ",",
      quoteStrings: '"',
      decimalSeparator: ".",
      showLabels: true,
      showTitle: true,
      title: "Rent",
      filename: "MonthlyRent",
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.rentDetails);
  }


  downloadCSV(str: string) {
    if (str === 'property') {
      this.getActiveProperty()
      const options = {
        fieldSeparator: ",",
        quoteStrings: '"',
        decimalSeparator: ".",
        showLabels: true,
        showTitle: true,
        title: "Properties",
        filename: "Active Properties",
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
        // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(this.result1);
    }

    if (str === 'owner') {
      this.getActiveOwner();
      const options = {
        fieldSeparator: ",",
        quoteStrings: '"',
        decimalSeparator: ".",
        showLabels: true,
        showTitle: true,
        title: "Owners",
        filename: "Active Owners",
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
        //headers: ['Owner Name', 'Owner Mail', 'State', 'City', 'User Type'] ///<-- Won't work with useKeysAsHeaders present!
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(this.result2);
    }

    if (str === 'tenant') {
      this.getActiveTenant()
      const options = {
        fieldSeparator: ",",
        quoteStrings: '"',
        decimalSeparator: ".",
        showLabels: true,
        showTitle: true,
        title: "Tenants",
        filename: "Active Tenants",
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(this.result3);
    }

    if (str === 'lease') {
      this.getLease();
      const options = {
        fieldSeparator: ",",
        quoteStrings: '"',
        decimalSeparator: ".",
        showLabels: true,
        showTitle: true,
        title: "Tenants",
        filename: "ActiveLease",
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(this.result5);
    }

    if (str === 'rent') {
      this.generateMonthlyRentReport();
      const options = {
        fieldSeparator: ",",
        quoteStrings: '"',
        decimalSeparator: ".",
        showLabels: true,
        showTitle: true,
        title: "Rent",
        filename: "MonthlyRent",
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(this.rentDetails);
    }
  }

  generatePDF(str: string) {
    if (str === 'property') {
      this.getActiveProperty();
      var doc = {
        pageSize: "A4",
        pageOrientation: "portrait",
        pageMargins: [40, 70, 50, 80],
        content: [],
        footer: function (currentPage, pageCount) {
          return [
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            },
            {
              columns: [
                {
                  text: currentPage.toString() + " | Page ",
                  bold: true,
                  alignment: "left",
                  margin: [40, 5, 0, 30]
                },
                {
                  text: "Property Management System",
                  alignment: "right",
                  margin: [0, 5, 40, 30],
                  italic: true,
                  color: "#9e9a99"
                }
              ]
            }
          ];
        },
        header: function (currentPage, pageCount, pageSize) {
          return [
            {
              text: "Property",
              alignment: "right",
              margin: [0, 5, 40, 30],
              italic: true,
              color: "#9e9a99"
            },
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            }

          ]
        }
      };
      let responseList = this.result1;
      let dataArray = [];
      //['prop_id', 'comp_id', 'prop_name', 'proptype', 'city_name', 'state_name', 'county_name', 'country_name', 'owner_fullname']
      dataArray.push([
        { text: "Property Name", bold: true },
        { text: "Property Type", bold: true },
        { text: "City", bold: true },
        { text: "State", bold: true },
        { text: "County", bold: true },
        { text: "Country", bold: true },
        { text: "Owner", bold: true }
      ]);
      responseList.map((value) => {
        dataArray.push([
          { text: value.prop_name, italics: true },
          { text: value.proptype, italics: true },
          { text: value.city_name, italics: true },
          { text: value.state_name, italics: true },
          { text: value.county_name, italics: true },
          { text: value.country_name, italics: true },
          { text: value.owner_fullname, italics: true }
        ]);
      });
      // responseList.forEach(el => {
      //   dataArray.push(el);
      // });
      doc.content.push({
        table: {
          headerRows: 1,
          body: dataArray
        }
      });
      pdfMake.createPdf(doc).download("ActiveProperties.pdf");
    }

    if (str === 'owner') {
      this.getActiveOwner();
      var doc = {
        pageSize: "A4",
        pageOrientation: "portrait",
        pageMargins: [40, 70, 50, 80],
        content: [],
        footer: function (currentPage, pageCount) {
          return [
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            },
            {
              columns: [
                {
                  text: currentPage.toString() + " | Page ",
                  bold: true,
                  alignment: "left",
                  margin: [40, 5, 0, 30]
                },
                {
                  text: "Property Management System",
                  alignment: "right",
                  margin: [0, 5, 40, 30],
                  italic: true,
                  color: "#9e9a99"
                }
              ]
            }
          ];
        },
        header: function (currentPage, pageCount, pageSize) {
          return [
            {
              text: "Owners",
              alignment: "right",
              margin: [0, 5, 40, 30],
              italic: true,
              color: "#9e9a99"
            },
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            }

          ]
        }
      };
      let list = this.result2;
      let dataArray = [];
      dataArray.push
        (
          [
            { text: "Owner Name", bold: true },
            { text: "Owner Address", bold: true },
            { text: "Owner Email", bold: true },
            { text: "Owner Phone", bold: true },
            { text: "Owner Zip", bold: true },
            { text: "State", bold: true },
            { text: "City", bold: true }
          ]
        );
      list.map((value) => {
        dataArray.push
          (
            [
              { text: value.owner_fullname, italics: true },
              { text: value.owner_address, italics: true },
              { text: value.owner_mail, italics: true },
              { text: value.owner_phone, italics: true },
              { text: value.owner_zip, italics: true },
              { text: value.state_name, italics: true },
              { text: value.city_name, italics: true }
            ]
          );
      });
      doc.content.push({
        table: {
          headerRows: 1,
          body: dataArray
        }
      });
      pdfMake.createPdf(doc).download('ActiveOwners.pdf');
    }

    if (str === 'tenant') {
      this.getActiveTenant();
      var doc = {
        pageSize: "A4",
        pageOrientation: "portrait",
        pageMargins: [40, 70, 50, 80],
        content: [],
        footer: function (currentPage, pageCount) {
          return [
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            },
            {
              columns: [
                {
                  text: currentPage.toString() + " | Page ",
                  bold: true,
                  alignment: "left",
                  margin: [40, 5, 0, 30]
                },
                {
                  text: "Property Management System",
                  alignment: "right",
                  margin: [0, 5, 40, 30],
                  italic: true,
                  color: "#9e9a99"
                }
              ]
            }
          ];
        },
        header: function (currentPage, pageCount, pageSize) {
          return [
            {
              text: "Tenants",
              alignment: "right",
              margin: [0, 5, 40, 30],
              italic: true,
              color: "#9e9a99"
            },
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            }

          ]
        }
      };

      let list = this.result3;
      let dataArray = [];
      dataArray.push
        (
          [
            { text: "First Name", bold: true, margin: [5, 2, 50, 20] },
            { text: "Last Name", bold: true, margin: [5, 2, 50, 20] },
            { text: "Email Address", bold: true, margin: [5, 2, 50, 20] },
            { text: "Type", bold: true, margin: [5, 2, 50, 20] },
            { text: "Contact Number", bold: true, margin: [5, 2, 50, 20] },
          ]
        );
      list.map((value) => {
        dataArray.push
          (
            [
              { text: value.tnt_fname, italics: true },
              { text: value.tnt_lname, italics: true },
              { text: value.tnt_mail, italics: true },
              { text: value.tnt_type, italics: true },
              { text: value.tnt_phone, italics: true },
            ]
          );
      });
      doc.content.push({
        table: {
          headerRows: 1,
          body: dataArray
        }
      });
      pdfMake.createPdf(doc).download('ActiveTenants.pdf');
    }

    if (str === 'lease') {
      this.getLease();
      var doc = {
        pageSize: "A4",
        pageOrientation: "portrait",
        pageMargins: [40, 70, 50, 80],
        content: [],
        footer: function (currentPage, pageCount) {
          return [
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            },
            {
              columns: [
                {
                  text: currentPage.toString() + " | Page ",
                  bold: true,
                  alignment: "left",
                  margin: [40, 5, 0, 30]
                },
                {
                  text: "Property Management System",
                  alignment: "right",
                  margin: [0, 5, 40, 30],
                  italic: true,
                  color: "#9e9a99"
                }
              ]
            }
          ];
        },
        header: function (currentPage, pageCount, pageSize) {
          return [
            {
              text: "Lease",
              alignment: "right",
              margin: [0, 5, 40, 30],
              italic: true,
              color: "#9e9a99"
            },
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            }

          ]
        }
      };

      let list = this.result5;
      let dataArray = [];
      dataArray.push
        (
          [
            { text: "Property Name", bold: true, margin: [5, 2, 50, 20] },
            { text: "Tenant Mail", bold: true, margin: [5, 2, 50, 20] },
            { text: "Tenant Type", bold: true, margin: [5, 2, 50, 20] },
            { text: "Start Date", bold: true, margin: [5, 2, 50, 20] },
            { text: "End Date", bold: true, margin: [5, 2, 50, 20] }
          ]
        );
      list.map((value) => {
        dataArray.push
          (
            [
              { text: value.prop_name, italics: true },
              { text: value.tnt_mail, italics: true },
              { text: value.tnt_type, italics: true },
              { text: this.datePipe.transform(value.cont_from, 'MM-dd-yyyy'), italics: true },
              { text: this.datePipe.transform(value.cont_to, 'MM-dd-yyyy'), italics: true },
            ]
          );
      });
      doc.content.push({
        table: {
          headerRows: 1,
          body: dataArray
        }
      });
      pdfMake.createPdf(doc).download('ActiveLease.pdf');
    }

    if (str === 'rent') {
      this.generateMonthlyRentReport();
      var doc = {
        pageSize: "A4",
        pageOrientation: "portrait",
        pageMargins: [40, 70, 50, 80],
        content: [],
        footer: function (currentPage, pageCount) {
          return [
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            },
            {
              columns: [
                {
                  text: currentPage.toString() + " | Page ",
                  bold: true,
                  alignment: "left",
                  margin: [40, 5, 0, 30]
                },
                {
                  text: "Property Management System",
                  alignment: "right",
                  margin: [0, 5, 40, 30],
                  italic: true,
                  color: "#9e9a99"
                }
              ]
            }
          ];
        },
        header: function (currentPage, pageCount, pageSize) {
          return [
            {
              text: "Tenants",
              alignment: "right",
              margin: [0, 5, 40, 30],
              italic: true,
              color: "#9e9a99"
            },
            {
              canvas: [
                { type: "line", x1: 30, y1: 0, x2: 555, y2: 0, lineWidth: 0.1 }
              ]
            }

          ]
        }
      };

      let list = this.rentDetails;
      let dataArray = [];
      dataArray.push
        (
          [
            { text: "Owner Name", bold: true },
            { text: "Property Address", bold: true },
            { text: "Income Amount", bold: true },
            { text: "Expense Amount", bold: true },
            { text: "Net Amount", bold: true },
            { text: "Period", bold: true },
          ]
        );
      list.map((value) => {
        dataArray.push
          (
            [
              { text: value.owner_name, italics: true },
              { text: value.property_address, italics: true },
              { text: value.income_amt, italics: true },
              { text: value.expense_amt, italics: true },
              { text: value.net_amt, italics: true },
              { text: value.period, italics: true }
            ]
          );
      });
      doc.content.push({
        table: {
          headerRows: 1,
          body: dataArray
        }
      });
      pdfMake.createPdf(doc).download('MonthlyRent.pdf');
    }
  }

  generate1099Form() {
    this.show1099 = true;
  }


}
