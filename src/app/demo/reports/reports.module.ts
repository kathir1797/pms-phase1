import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { ReportsRoutingRoutes } from './reports-routing.routing';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker/';
import { ExportAsModule } from 'ngx-export-as';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReportsRoutingRoutes,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    NgbDatepickerModule,
    BsDatepickerModule,
    ExportAsModule
  ],
  declarations: [ReportsComponent],
  providers :[DatePipe]
})
export class ReportsModule { }
