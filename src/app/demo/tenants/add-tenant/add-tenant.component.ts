import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TenantsService } from 'src/app/Services/tenants.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { GetTenant } from 'src/app/common_models/tenant_model';
import { tap, switchMap } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-tenant',
  templateUrl: './add-tenant.component.html',
  styleUrls: ['./add-tenant.component.css']
})
export class AddTenantComponent implements OnInit {

  obj = localStorage.getItem('currentuser');
  ddlistname = new Array;
  showlease: boolean;
  tenantList = new GetTenant();
  tenant = new Array();
  url: any;
  isSubmitted: boolean;
  isShowCheckBox: boolean = false;
  public maskTeleUS = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private ts: TenantsService, private router: Router, private toastr: ToastrService, private ddlist: DdlistService) { }

  ngOnInit() {
    this.getDDlistName('Tenant_Type').subscribe(response => { console.log(response) });
    this.url = this.router.url;
    this.url = this.url.split('/');
    if (this.url[3] == 'edittenant') {
      this.getTenant();
      this.isShowCheckBox = true;
      this.form.get('p_active').value
    } else {
      this.isSubmitted = true;
      this.isShowCheckBox = false;
    }
  }



  form = new FormGroup({
    p_tenant_id: new FormControl(-1),
    p_comp_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).comp_id),
    p_tnt_fname: new FormControl('', Validators.required),
    p_tnt_mname: new FormControl(''),
    p_tnt_lname: new FormControl('', Validators.required),
    p_tnt_type: new FormControl('', Validators.required),
    p_tnt_email: new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    p_tnt_phone: new FormControl('', Validators.required),
    p_active: new FormControl(true),
    p_usr_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).usr_id),
  });

  get f() {
    return this.form.controls;
  }

  getDDlistName(listname: string) {
    return this.ddlist.getDDlist(listname)
      .pipe(
        /* use tap() to set `this.ddlistname` */
        tap(response => {
          if (response.success) {
            this.ddlistname = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  addTenant() {
    if (this.form.valid) {
      const formValue = this.form.value
      formValue.p_tnt_phone = new RegExp(this.form.get('p_tnt_phone').value).source.replace(/-/g, "");
      this.ts.addTenant(JSON.stringify(formValue)).subscribe(response => {
        if (response.success) {
          this.showlease = true;
          if (this.form.controls['p_active'].value == false) {
            Swal.fire('Tenant', 'Deactivated Successfully');
          }
          this.toastr.success('Saved Successfully', "Tenant");
        }
        else {
          this.toastr.error(response.errorMessage, 'Tenant');
        }
      });
    } else {
      this.validateAllFormFields(this.form);
      this.toastr.error('Required Field information is not available. Please enter required information in highlighted fields as below.', 'Tenant');
    }
  }

  getTenant() {
    this.tenantList.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.tenantList.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.tenantList.p_tnt_id = this.url[4];
    this.getDDlistName('Tenant_Type').pipe(
      switchMap(result => {
        if (result.success) {
          this.ddlistname = result.data;
        }
        return this.ts.getTenant(this.tenantList)
      })
    ).subscribe(response => {
      if (response.success) {
        let data = response.data;
        let filteredData = this.ddlistname.filter(value => {
          if (value.dd_value == data[0].tnt_type) {
            return value
          }
        });
        this.form.patchValue({
          p_tenant_id: data[0].tenant_id,
          p_comp_id: data[0].comp_id,
          p_tnt_fname: data[0].tnt_fname,
          p_tnt_lname: data[0].tnt_lname,
          p_tnt_type: filteredData[0].dd_det_id,
          p_tnt_email: data[0].tnt_mail,
          p_tnt_phone: data[0].tnt_phone,
          p_active: data[0].active,
          p_usr_id: JSON.parse(localStorage.getItem('currentuser')).usr_id
        })
        // this.form.get('p_tenant_id').setValue(data[0].tenant_id);
        // this.form.get('p_comp_id').setValue(data[0].comp_id);
        // this.form.get('p_tnt_fname').setValue(data[0].tnt_fname);
        // this.form.get('p_tnt_lname').setValue(data[0].tnt_lname);
        // this.form.get('p_tnt_email').setValue(data[0].tnt_mail);
        // this.form.get('p_tnt_phone').setValue(data[0].tnt_phone);
        // this.form.get('p_active').setValue(data[0].active);
        // this.form.patchValue({ p_tnt_type: filteredData[0].dd_det_id });
        // this.form.get('p_usr_id').setValue(JSON.parse(localStorage.getItem('currentuser')).usr_id)
      } else {
        this.toastr.error(response.errorMessage)
      }
    });
  }

}
