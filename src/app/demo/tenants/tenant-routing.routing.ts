import { Routes, RouterModule } from '@angular/router';
import { TenantsComponent } from './tenants.component';
import { AddTenantComponent } from './add-tenant/add-tenant.component';

const routes: Routes = [
  { 
    path : '',
    component : TenantsComponent
   },
   {
     path : 'addtenant',
     component : AddTenantComponent
   },
   {
     path :'edittenant/:id',
     component : AddTenantComponent
   }
];

export const TenantRoutingRoutes = RouterModule.forChild(routes);
