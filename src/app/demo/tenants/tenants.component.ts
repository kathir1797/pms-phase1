import { Component, OnInit } from '@angular/core';
import { TenantsService } from 'src/app/Services/tenants.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { GetTenant } from 'src/app/common_models/tenant_model';

@Component({
  selector: 'app-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.css']
})
export class TenantsComponent implements OnInit {

  searchText: string;
  public tenantList = new GetTenant();
  tenant = new Array();
  totalRecords: number;
  p: number = 1;

  constructor(public ts: TenantsService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.getTenant();
  }

  editTenant(tenantId : number) {
    console.log(tenantId);
    this.router.navigate(['/tenant/add/edittenant', tenantId])
  }

  getTenant() {
    this.tenantList.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.tenantList.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.tenantList.p_tnt_id = null;
    this.ts.getTenant(this.tenantList).subscribe(response =>{
      if(response.success) {
        console.log(response.data);
        this.tenant = response.data;
        this.totalRecords = response.data.length;
      } else {
        this.toastr.info('No Data Found', 'Tenant');
      }
    });
  }
}