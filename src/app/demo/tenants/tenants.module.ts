import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TenantsComponent } from './tenants.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { TenantRoutingRoutes } from './tenant-routing.routing';
import { AddTenantComponent } from './add-tenant/add-tenant.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { TextMaskModule } from 'angular2-text-mask';
/*import {CurrencyMaskModule} from 'ng2-currency-mask';*/
import { NgNumberFormatterModule } from 'ng-number-formatter';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TenantRoutingRoutes,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    TextMaskModule,
    /*CurrencyMaskModule,*/
    NgNumberFormatterModule
  ],
  declarations: [TenantsComponent, AddTenantComponent]
})
export class TenantsModule { }
