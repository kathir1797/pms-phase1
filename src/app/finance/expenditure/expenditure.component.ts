import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GetProperty } from 'src/app/common_models/property_model';
import { PropertyService } from 'src/app/Services/property.service';
import { ToastrService } from 'ngx-toastr';
import { DdlistService } from 'src/app/Services/ddlist.service';
import { tap } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FinanceServiceService } from 'src/app/Services/finance-service.service';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-expenditure',
  templateUrl: './expenditure.component.html',
  styleUrls: ['./expenditure.component.css']
})
export class ExpenditureComponent implements OnInit {

  url: any;
  public getprop = new GetProperty();
  propdet = new Array();
  ddlistname = new Array();
  fileContentBuffer: any;
  @ViewChild('fileUploader') fileUploader: ElementRef<HTMLInputElement>;

  constructor(private cs: PropertyService, private toastr: ToastrService, public ddlist: DdlistService, private fs: FinanceServiceService, private router: Router) {
    this.url = this.router.url;
    this.url = this.url.split('/');
  }

  ngOnInit() {
    this.getDDlistName("Expenditure_Type").subscribe(response => { });
    this.getProperty().subscribe(response => { });
    if (this.url[3] === 'propexp') {
      this.getPropertyById().subscribe(reponse => { });
    }
  }

  expenseForm = new FormGroup({
    p_exp_id: new FormControl(-1),
    p_comp_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).comp_id),
    p_exp_type: new FormControl('', Validators.required),
    p_prop_id: new FormControl('', Validators.required),
    p_exp_amt: new FormControl('', Validators.required),
    p_recpt_num: new FormControl('', Validators.required),
    p_comments: new FormControl(null),
    p_file_name: new FormControl(''),
    p_doc_img: new FormControl(''),
    p_active: new FormControl(true),
    p_usr_id: new FormControl(JSON.parse(localStorage.getItem('currentuser')).usr_id)
  });

  get f() {
    return this.expenseForm.controls;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  getPropertyById() {
    this.getprop.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getprop.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.getprop.p_prop_id = parseInt(this.url[4]);
    return this.cs.getProperty(this.getprop).pipe(
      tap(response => {
        if (response.success) {
          this.propdet = response.data;
          console.log(this.propdet);
          this.expenseForm.patchValue({ p_prop_id: this.propdet[0].prop_id });
        } else {
          this.toastr.info(response.errorMessage, 'Property')
        }
      })
    );
  }

  getProperty() {
    this.getprop.p_comp_id = JSON.parse(localStorage.getItem('currentuser')).comp_id;
    this.getprop.p_usr_id = JSON.parse(localStorage.getItem('currentuser')).usr_id;
    this.getprop.p_prop_id = null;
    return this.cs.getProperty(this.getprop).pipe(
      tap(response => {
        if (response.success) {
          console.log(response);
          this.propdet = response.data;
        } else {
          this.toastr.info(response.errorMessage, 'Property')
        }
      })
    );
  }

  getDDlistName(listname: string) {
    return this.ddlist.getDDlist(listname)
      .pipe(
        /* use tap() to set `this.ddlistname` */
        tap(response => {
          if (response.success) {
            this.ddlistname = response.data;
          } else {
            this.toastr.error(response.errorMessage);
          }
        })
      );
  }

  fileToBase64 = file => {
    return new Promise(resolve => {
      var reader = new FileReader();
      // Read file content on file loaded event
      reader.onload = function (event) {
        resolve(reader.result);
      };
      // Convert data to base64
      reader.readAsDataURL(file);
    });
  };

  getBase64(event) {
    //console.log(event.target.files[0].name.substring(event.target.files[0].name.lastIndexOf('.')+1));
    let file = event.target.files[0];
    let name = file.name
    console.log(name);
    this.fileToBase64(file).then(results => {
      this.fileContentBuffer = results;
      this.expenseForm.patchValue({
        p_doc_img: this.fileContentBuffer,
        p_file_name: name
      });
    });
  }

  resetFileUploader() {
    this.fileUploader.nativeElement.value = null;
  }

  addExpense() {
    var x = this.fileUploader.nativeElement.value
    console.log(this.expenseForm.valid)
    if (this.expenseForm.valid) {
      this.fs.addExpense(JSON.stringify(this.expenseForm.value)).subscribe(response => {
        if (response.success) {
          this.toastr.success('Saved Successfully', 'Property Expense');
          this.expenseForm.reset();
          this.resetFileUploader();
        } else {
          this.toastr.error(response.errorMessage, 'Property Expense');
        }
      });
    } else {
      this.validateAllFormFields(this.expenseForm);
      this.toastr.error('Required Field information is not available. Please enter required information in highlighted fields as below.', 'Property Expense');
    }
  }

}
