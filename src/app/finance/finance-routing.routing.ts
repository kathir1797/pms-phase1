import { Routes, RouterModule } from '@angular/router';
import { IncomeComponent } from './income/income.component';
import { ExpenditureComponent } from './expenditure/expenditure.component';

const routes: Routes = [
  {
    path: 'income',
    component: IncomeComponent
  },
  {
    path: 'expenditure',
    component: ExpenditureComponent
  },
  {
    path: 'income/propinc/:propId',
    component: IncomeComponent
  },
  {
    path: 'expenditure/propexp/:propId',
    component: ExpenditureComponent
  },
];

export const FinanceRoutingRoutes = RouterModule.forChild(routes);
