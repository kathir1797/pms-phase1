import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinanceComponent } from './finance.component';
import { SharedModule } from '../theme/shared/shared.module';
import { FinanceRoutingRoutes } from './finance-routing.routing';
import { ExpenditureComponent } from './expenditure/expenditure.component';
import { IncomeComponent } from './income/income.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FinanceRoutingRoutes
  ],
  declarations: [FinanceComponent, IncomeComponent, ExpenditureComponent]
})
export class FinanceModule { }
