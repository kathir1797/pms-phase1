import { Component, DoCheck, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { animate, style, transition, trigger } from '@angular/animations';
import { DattaConfig } from '../../../../../app-config';
import { Router } from '@angular/router';
import { ChangePassword } from 'src/app/common_models/user_model';
import { AuthSigninService } from 'src/app/Services/auth-signin.service';
import { isNullOrUndefined } from 'util';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-nav-right',
  templateUrl: './nav-right.component.html',
  styleUrls: ['./nav-right.component.scss'],
  providers: [NgbDropdownConfig],
  animations: [
    trigger('slideInOutLeft', [
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('300ms ease-in', style({ transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        animate('300ms ease-in', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('slideInOutRight', [
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('300ms ease-in', style({ transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        animate('300ms ease-in', style({ transform: 'translateX(-100%)' }))
      ])
    ])
  ]
})
export class NavRightComponent implements OnInit, DoCheck {
  public visibleUserList: boolean;
  public chatMessage: boolean;
  public friendId: boolean;
  public dattaConfig: any;
  usrobj = localStorage.getItem('currentuser');
  public username = JSON.parse(this.usrobj).usr_name;
  showResetPassword: boolean;
  cp = new ChangePassword();
  reEnterPswd: string;

  constructor(config: NgbDropdownConfig, private router: Router, private auth: AuthSigninService, private toastr: ToastrService) {
    config.placement = 'bottom-right';
    this.visibleUserList = false;
    this.chatMessage = false;
    this.dattaConfig = DattaConfig.config;
  }

  ngOnInit() {
  }

  onChatToggle(friend_id) {
    this.friendId = friend_id;
    this.chatMessage = !this.chatMessage;
  }

  ngDoCheck() {
    if (document.querySelector('body').classList.contains('datta-rtl')) {
      this.dattaConfig['rtl-layout'] = true;
    } else {
      this.dattaConfig['rtl-layout'] = false;
    }
  }

  logOut() {
    localStorage.clear();
    this.router.navigate(['/auth/signin']);
  }

  resetPassword() {
    this.showResetPassword = true;
  }
  changePassword() {
    this.cp.p_login_usr_id = JSON.parse(this.usrobj).usr_id;
    this.cp.p_reset_by_usr_id = JSON.parse(this.usrobj).usr_id;
    if (isNullOrUndefined(this.cp.p_usr_pswd_old) && isNullOrUndefined(this.cp.p_usr_pswd_new)) {
      this.toastr.warning('Please fill all fields');
    } else if (isNullOrUndefined(this.cp.p_usr_pswd_old)) {
      this.toastr.warning('Please Enter Current Password');
    } else if (isNullOrUndefined(this.cp.p_usr_pswd_new)) {
      this.toastr.warning('New Password cannot be blank or undefined');
    } else if (isNullOrUndefined(this.cp.p_usr_pswd_new)) {
      this.toastr.warning('New Password cannot be blank or undefined');
    } else {
      if (this.cp.p_usr_pswd_new == this.reEnterPswd) {
        this.auth.resetPassword(this.cp).subscribe(response => {
          if (response.success) {
            this.toastr.success('Password Changed Successfully');
            this.router.navigate(['/auth/signin']);
          } else {
            this.toastr.error(response.errorMessage);
          }
        });
      } else {
        this.toastr.error('New Password Didnt Match, Try Again');
      }
    }
  }
}
