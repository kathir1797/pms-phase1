import { Injectable } from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

// const NavigationItems = [
//   {
//     id: 'navigation',
//     title: 'DashBoard',
//     type: 'group',
//     icon: 'icon-navigation',
//     children: [
//       {
//         id: 'dashboard',
//         title: 'Dashboard',
//         type: 'collapse',
//         icon: 'feather icon-home',
//         children: [
//           {
//             id: 'default',
//             title: 'Add New Client',
//             type: 'item',
//             url: '/default/adduser'
//           },
//           {
//             id: 'default',
//             title: 'Edit/View Client',
//             type: 'item',
//             url: '/default/adduser/search'
//           },
//         ]
//       },
//     ]
//   },
//   {
//     id: 'navigation',
//     title: 'Dashboard',
//     type: 'group',
//     icon: 'icon-navigation',
//     children: [
//       {
//         id: 'dashboard',
//         title: 'DashBoard',
//         type: 'collapse',
//         icon: 'feather icon-home',
//         children: [
//           {
//             id: 'default',
//             title: 'Create New User',
//             type: 'item',
//             url: '/manager/add',
//           },
//           {
//             id: 'default',
//             title: 'Edit/Delete User',
//             type: 'item',
//             url: '/manager/add/view'
//           }
//         ]
//       },
//     ]
//   },
//   {
//     id: 'navigation',
//     title: 'Properties',
//     type: 'item',
//     icon: 'icon-navigation',
//     url: '/property/add'
//   },
//   {
//     id: 'navigation',
//     title: 'Owners',
//     type: 'item',
//     icon: 'icon-navigation',
//     url: '/owner/add'
//   }
// ];
const sysAdmin = [{
  id: 'navigation',
  title: 'DashBoard',
  type: 'group',
  icon: 'icon-navigation',
  children: [
    {
      id: 'dashboard',
      title: 'Dashboard',
      type: 'collapse',
      icon: 'feather icon-home',
      children: [
        {
          id: 'default',
          title: 'Add New Client',
          type: 'item',
          url: '/default/adduser'
        },
        {
          id: 'default',
          title: 'Edit/View Client',
          type: 'item',
          url: '/default/adduser/search'
        },
      ]
    },
  ]
}
];

const compAdmin = [
  {
    id: 'navigation',
    title: 'Dashboard',
    type: 'group',
    icon: 'icon-navigation',
    children: [
      {
        id: 'dashboard',
        title: 'DashBoard',
        type: 'collapse',
        icon: 'feather icon-home',
        children: [
          {
            id: 'default',
            title: 'Create New User',
            type: 'item',
            url: '/manager/add',
          },
          {
            id: 'default',
            title: 'Edit/Delete User',
            type: 'item',
            url: '/manager/add/view'
          }
        ]
      },
    ]
  },
];

const manager = [
  {
    id: 'navigation',
    title: 'Properties',
    type: 'item',
    icon: 'icon-navigation',
    url: '/property/add'
  },
  {
    id: 'navigation',
    title: 'Owners',
    type: 'item',
    icon: 'icon-navigation',
    url: '/owner/add'
  },
  {
    id: 'navigation',
    title: 'Tenants',
    type: 'item',
    icon: 'icon-navigation',
    url: '/tenant/add'
  },
  {
    id: 'navigation',
    title: 'Lease',
    type: 'item',
    icon: 'icon-navigation',
    url: '/lease/search'
  },
  {
    id: 'navigation',
    title: 'Reports',
    type: 'item',
    icon: 'icon-navigation',
    url: '/reports'
  },
  {
    id: 'navigation',
    title: 'Finance',
    type: 'group',
    icon: 'icon-navigation',
    children: [
      {
        id: 'dashboard',
        title: 'Finance',
        type: 'collapse',
        icon: 'feather icon-home',
        children: [
          {
            id: 'default',
            title: 'Add Income',
            type: 'item',
            url: '/finance/income',
          },
          {
            id: 'default',
            title: 'Add Expenditure',
            type: 'item',
            url: '/finance/expenditure'
          }
        ]
      },
    ]
  }
]

@Injectable()
export class NavigationItem {
  getSysAdmin() {
    return sysAdmin;
  }

  getCompanyAdmin() {
    return compAdmin;
  }

  getManager() {
    return manager;
  }
}
